-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.25 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп данных таблицы vm-project.blogs: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
REPLACE INTO `blogs` (`id`, `title`, `text`, `image`, `lang`, `description`, `keywords`, `id_author`, `created_at`, `updated_at`) VALUES
	(1, 'gfgfg', 'gfgfgfg', NULL, 'ru', 'fgfgfgf', NULL, 1, '2019-03-29 18:59:54', '2019-03-29 18:59:54');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;

-- Дамп данных таблицы vm-project.calls: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `calls` DISABLE KEYS */;
REPLACE INTO `calls` (`id`, `name`, `subject`, `phone`, `email`, `viewed`, `created_at`, `updated_at`) VALUES
	(1, 'hfhfhf', 'hfhfhf', 'hfhfhf', 'hfhfhf', '0', '2018-10-11 14:03:03', '2018-10-11 14:03:03'),
	(2, 'sfsfs', 'fsfsfs', 'fsfsfs', 'fsfsf', '1', '2018-10-11 14:05:42', '2018-10-12 11:34:22'),
	(3, 'fsfsffs', 'fsfsfsf', 'fsfsfsf', 'fsfsfsf', '1', '2018-10-11 14:07:24', '2018-10-11 14:07:24'),
	(4, 'fsfsfs', 'gdgdgdgdggdgdgdgdgdg', '42424244242424', 'zip14.05.1996@gmail.com', '0', '2018-10-11 14:21:35', '2018-10-11 14:21:35'),
	(5, 'gdgdgdg', 'gdfgdgdg gdgdgdgd gdgdgd', '3453535353', 'zip14.05.1996@gmail.com', '0', '2018-10-17 08:52:10', '2018-10-17 08:52:10'),
	(6, 'gdgdgdg', 'gdfgdgdg gdgdgdgd gdgdgd', '3453535353', 'zip14.05.1996@gmail.com', '0', '2018-10-17 08:52:33', '2018-10-17 08:52:33'),
	(7, 'bcbcbcb', 'hgfhfhfhfh hfhfhfhfhf', '535353453535', 'admin@admin.com', '0', '2018-10-17 08:53:12', '2018-10-17 08:53:12'),
	(8, 'hfhfhfhfhfhhf', '756756757567567575775', '565475757', 'zip14.05.1996@gmail.com', '0', '2018-10-17 08:53:31', '2018-10-17 08:53:31'),
	(9, 'hfhfhfhfh', 'hfghfh hfhfhf hfhfhf', '554646464564', 'admin@admin.com', '0', '2018-10-17 08:56:00', '2018-10-17 08:56:00');
/*!40000 ALTER TABLE `calls` ENABLE KEYS */;

-- Дамп данных таблицы vm-project.migrations: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(6, '2014_10_12_000000_create_users_table', 1),
	(7, '2014_10_12_100000_create_password_resets_table', 1),
	(8, '2018_10_09_075001_create_calls_table', 1),
	(9, '2018_10_09_075659_create_blogs_table', 1),
	(10, '2018_10_09_082359_create_practices_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп данных таблицы vm-project.password_resets: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп данных таблицы vm-project.practices: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `practices` DISABLE KEYS */;
REPLACE INTO `practices` (`id`, `title`, `text`, `image`, `lang`, `description`, `keywords`, `created_at`, `updated_at`) VALUES
	(1, 'Lorem Ipsum is simply dummy text 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '5bc47c5552dee.jpg', 'ru', 'Lorem Ipsum is simply dummy text', NULL, '2018-10-15 11:39:01', '2018-10-15 11:39:01'),
	(2, 'Lorem Ipsum is simply dumm 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '5bc47c9b8b78b.jpg', 'ro', 'Lorem Ipsum is simply dumm', NULL, '2018-10-15 11:40:11', '2018-10-15 11:40:11');
/*!40000 ALTER TABLE `practices` ENABLE KEYS */;

-- Дамп данных таблицы vm-project.users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `name`, `surname`, `login`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin', 'admin', 'admin@admin.com', '$2y$10$l59gEI22sqUEV7iOLZzePegXa2avWYpAtO4SUrOglsFpUIF3FcP6q', 'zKp9aXlK7QjPY8J3mY7l4zmC4tjqD9duXdhgBx3Hkad0Igoj8USvnyvKvh7L', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
