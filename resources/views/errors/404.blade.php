@extends('layouts.main')

@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>Pagina 404</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
        
    <!--inner-content start-->
    <div class="inner-content innerbg">
        <div class="container"> 
        
            <!-- 404 start -->
            <div class="inner-wrap">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="four-zero-page">
                            <h2>404</h2>
                            <h3>Ne pare rău că pagina nu a fost găsită</h3>
                            <p>Pagina pe care o căutați nu este disponibilă sau a fost eliminată. 
                                Încercați să mergeți la pagina anterioară utilizând butonul de mai jos.</p>
                            <div class="view-btn"> 
                                <a href="{{route('page.home')}}">Reveniți pe pagina de acasa</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 404 end --> 
        </div>
    </div>
    <!--inner-content end--> 
      

@endsection