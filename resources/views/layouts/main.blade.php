<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="{{!empty($description) ? $description : __('words.site_description')}}" />
  <meta name="author" content="Zagorcea Ion" />

  <title>{{!empty($title) ? $title : __('words.site_name')}}</title>

  @if(!empty($article_og))
    <meta property="og:url" content="{{ $article_og['url'] }}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $article_og['title'] }}">
    <meta property="og:description" content="{{ $article_og['description'] }}">
    <meta property="og:image" content="{{ $article_og['image'] }}">
  @else
    <meta property="og:image" content="{{ asset('images/logo.png') }}">
  @endif

  @if (config('app.env') == 'production')
    <meta name="google-site-verification" content="0wPHbUzy4UXcm13PefIeX-lSrJGFpwN87JdEqVp2Ppg" />
  @endif

  <!-- Fav Icon -->
  <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

  <!-- Bootstrap -->
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('rs-plugin/css/settings.css')}}" rel="stylesheet">
  <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">

  <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
  <link href="{{asset('plugins/Validation-Engine/css/validationEngine.jquery.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('plugins/noty/css/noty.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('plugins/noty/themes/mint.css')}}" rel="stylesheet" type="text/css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="{{asset('plugins/Validation-Engine/js/jquery.validationEngine.js')}}"></script>
  <script src="{{asset('plugins/Validation-Engine/js/jquery.validationEngine-ro.js')}}"></script>

  @if (config('app.env') == 'production')
     <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NSYLZ8X1TV"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-NSYLZ8X1TV');
    </script>
  @endif

</head>
<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v7.0&appId=1027156657756194&autoLogAppEvents=1"></script>

<!-- Topbar Start-->
<div id="topbar" class="site-topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-8">
        <ul class="bar-info">
          <li><i class="fa fa-phone"></i><a href="tel:+37369363364" class="clear-style-link">069 363 364</a></li>
          <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@lawoffice.md" class="clear-style-link">info@lawoffice.md</a></li>
        </ul>
      </div>
      <!-- <div class="col-md-6 col-sm-4">
        <ul class="topbar-links">
          <li>
            <a href="<?= route('setlocale', ['lang' => 'ro']) ?>">
              <img src="{{asset('images/ro.png')}}" style="width:20px; padding-right:5px; margin-top:-2px">RO
            </a>
          </li>
          <li>
            <a href="<?= route('setlocale', ['lang' => 'ru']) ?>">
                <img src="{{asset('images/ru.png')}}" style="width:20px; padding-right:5px; margin-top:-2px">RU
            </a>
        </ul>
      </div> -->
    </div>
  </div>
</div>
<!-- Topbar End --> 

<!-- Header Start-->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-3">
        <div class="logo"> <a href="{{route('page.home')}}"><img src="{{asset('images/logo.png')}}" alt="" style="padding-top:10px"/> </a></div>
      </div>
      <div class="col-md-8 col-sm-9">
        <div class="navigationwrape">
          <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li> <a href="{{route('page.home')}}" class="{{!empty($active_page) &&  $active_page == 'homePage' ? 'active' : ''}}">{{__('words.nav_home')}}</a></li>
                <li> <a href="{{route('page.about')}}" class="{{!empty($active_page) &&  $active_page == 'aboutUs' ? 'active' : ''}}">{{__('words.nav_about')}}</a></li>
                <li> <a href="{{route('page.services')}}" class="{{!empty($active_page) &&  $active_page == 'services' ? 'active' : ''}}">{{__('words.nav_services')}}</a></li>
                <li> <a href="{{route('page.articles')}}" class="{{!empty($active_page) &&  $active_page == 'articles' ? 'active' : ''}}">{{__('words.nav_articles')}}</a></li>
                <li> <a href="{{route('page.consultation')}}" class="{{!empty($active_page) &&  $active_page == 'contact' ? 'active' : ''}}">{{__('words.nav_contact')}}</a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header End--> 

@yield('content')

<!-- ContactWrp start -->
<div class="contact-wrap">
  <div class="contact-left">
    <div class="container">
      <div class="row contact-info">
        <div class="col-md-6">
          <div class="headingTitle">
            <h1>{!! __('words.footer_contact_title') !!}</h1>
          </div>
          <p>{{__('words.footer_contact_description')}}</p>
          <h4>{{__('words.footer_contact_information')}}</h4>
          <div class="address">{!! __('words.footer_contact_address') !!}</div>
          <div class="phone">069 363 364</div>
          <div class="email">
            <a href="mailto:info@lawoffice.md" class="clear-style-link">info@lawoffice.md</a>
          </div>
          <div class="appointment"><a href="{{route('page.consultation')}}">{{__('words.consultation')}}</a></div>
        </div>
      </div>
    </div>
    <div class="right-map">
      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2682.4050997262884!2d27.92484321589026!3d47.75419628576299!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40cb672f2bba7459%3A0x7727dbe60e49dddb!2zU3RyYWRhIFB1yJlraW4gNjIsIELEg2zIm2kgMzEwMCwg0JzQvtC70LTQsNCy0LjRjw!5e0!3m2!1sru!2s!4v1557131603722!5m2!1sru!2s" width="600" height="575" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
<!-- ContactWrp end --> 

<!-- footer start -->

<div class="footer-wrap">
  <div class="container">
      <div class="row">
      <div class="col-md-{{!empty($footerServices) ? 4 : 6}}">
        <div class="left-col">
          <div class="footer-logo"><img src="{{asset('images/footer-logo.png')}}" alt=""></div>
          <p>{{__('words.site_description')}}</p>
          <ul class="footer-icons">
            <li><a href="https://www.facebook.com/Violina-Munteanu-108618654268408" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/violinamunteanu/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

          </ul>
        </div>
      </div>
      <div class="col-md-{{!empty($footerServices) ? 4 : 6}} col-sm-6">
        <div class="footer-heading">{{__('words.navigation')}}</div>
        <ul class="footer-nav">
          <li> <a href="{{route('page.home')}}">{{__('words.nav_home')}}</a></li>
          <li> <a href="{{route('page.about')}}">{{__('words.nav_about')}}</a></li>
          <li> <a href="{{route('page.services')}}">{{__('words.nav_services')}}</a></li>
          <li> <a href="{{route('page.articles')}}">{{__('words.nav_articles')}}</a></li>
          <li> <a href="{{route('page.consultation')}}">{{__('words.nav_contact')}}</a></li>
        </ul>
      </div>
        @if(!empty($footerServices))
          <div class="col-md-4 col-sm-6">
            <div class="footer-heading">{{__('words.nav_services')}}</div>
            <ul class="footer-nav">
              @foreach($footerServices as $item)
                <li><a href="{{route('page.serviceDetails', ['service' => $item->alias])}}">{{$item->title}}</a></li>
              @endforeach
            </ul>
          </div>
        @endif
    </div>
    <div class="footer-service">
      <div class="copyright">Copyright © {{date('Y')}} Violina M.Munteanu, All Rights Reserved</div>
    </div>
  </div>
</div>

<!-- footer end --> 

<!--page scroll start-->
<div class="page-scroll scrollToTop"><a href="{{route('page.home')}}#"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></div>
<!--page scroll start-->

<!--page scroll start-->
<div class="page-scroll scrollToTop"><a href="{{route('page.home')}}#"><i class="fa fa-arrow-up" aria-hidden="true"></i></a></div>
<!--page scroll start-->


<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="{{asset('plugins/noty/js/noty.js')}}"></script>
<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Load JS siles --> 
<script src="{{asset('js/owl.carousel.js')}}"></script>
<!-- SLIDER REVOLUTION SCRIPTS  --> 
<script src="{{asset('rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<!-- general script file --> 
<script src="{{asset('js/script.js')}}"></script>
 
</body>
</html>