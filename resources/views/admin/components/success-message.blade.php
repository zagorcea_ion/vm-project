@if (Session::has('status') && Session::has('message'))
    <script>
        $(document).ready(function() {
            new Noty({type: '{{Session::get('status')}}', layout: 'topRight', text:  '{{Session::get('message')}}', timeout:3000}).show();
        })
       
    </script>
@endif
