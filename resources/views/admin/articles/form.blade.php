@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="panel-title">{{!empty($article->id) ? 'Editarea' : 'Adăuga'}} articol</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" action="{{!empty($article->id) ? route('articles.update', ['article' => $article->id]) : route('articles.store')}}" class="form-horizontal" id="article_form" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <?php if(!empty($article->id)){?>
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" id='id' name="id" value="{{$article->id}}">
                    <?php }?>

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control validate[required, maxSize[250]]" id="title" name="title" placeholder="Title:" value="{{!empty($article->title) ? $article->title : ''}}">
                    </div>

                    <div class="form-group">
                        <label for="description">Descriere</label>
                        <textarea class="form-control validate[required, maxSize[250]]" id="description" name="description" rows="4" cols="80" style="resize: none" placeholder="Descriere:">{{!empty($article->description) ? $article->description : ''}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="editor">Text</label>
                        <textarea class="validate[required]" id="editor" name="text" rows="10" cols="80">{{!empty($article->text) ? $article->text : ''}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Limba</label>
                        <select class="form-control validate[required]" name="lang">
                            @if(empty($article->lang))
                                <option value="" selected>Selecteaza limba</option>
                            @endif

                            <option value="ru" {{ (!empty($article->lang) && $article->lang == "ru") ? "selected" : "" }}>RU</option>
                            <option value="ro" {{ (!empty($article->lang) && $article->lang == "ro") ? "selected" : "" }}>RO</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="image">Imaginea</label>
                            <input type="file" id="image" name="image">

                        </div>

                        @if(isset($article->image) && !empty($article->image))
                            <img src="/public/images/articles/{{ $article->image }}" width="400px" height="200px">
                            <input type="hidden" value="{{ $article->image }}" name="old_image">
                        @elseif(isset($article->id) && !empty($article->id))
                            <img src="/public/images/noImg.jpg" width="200px">
                        @endif

                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="video">Video</label>
                            <input type="text" class="form-control" id="video" name="youtube_video" value="{{!empty($article->youtube_video) ? $article->youtube_video : ''}}" placeholder="Introduceti ID pentru video de pe YouTube:">
                            <span class="help-block">Introduceti ID pentru video de pe YouTube</span>
                        </div>

                        @if(isset($article->youtube_video) && !empty($article->youtube_video))
                            <iframe width="400px" height="200px" src="https://www.youtube.com/embed/{{ $article->youtube_video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        @endif

                    </div>

                    <button class="btn btn-default" type="reset">Curăța</button>
                    <button class="btn btn-primary pull-right" type="submit">Adăuga</button>

                </form>
            </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>

    <script>
        $( document ).ready(function() {
            tinymce.init({
                selector: '#editor',
                entity_encoding: 'raw',
                menubar: false,
                branding: false,
                height: 500,
                max_height: 700,
                min_height: 500,
                plugins: [
                    'autolink autoresize fullscreen link lists paste',
                ],
                toolbar: 'undo redo | bold italic | underline strikethrough | bullist numlist | link | fullscreen',

                link_assume_external_targets: true,
                relative_urls: false,
                image_advtab: true ,
                remove_script_host: false,
                force_br_newlines: false,
                force_p_newlines: false,
                forced_root_block: "",
                extended_valid_elements: "br",
                verify_html: false,
                valid_children: "br",
                paste_as_text: true,

            });

            $("#article_form").validationEngine({
                promptPosition : "topLeft",
                onValidationComplete: function(form, status){
                    if(status){

                        $.ajax ({
                            url : $('#article_form').attr('action'),
                            type: 'POST',
                            data: new FormData($('#article_form')[0]),
                            dataType: 'JSON',
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false, // NEEDED, DON'T OMIT THIS
                            success: function (response) {
                                new Noty({type: 'success', layout: 'topRight', text: response.message, timeout:3000}).show();
                                document.location.href = '{{ route('articles.index') }}';

                                if(dt) {
                                    dt.draw(false)
                                }
                            },
                            error: function (error) {
                                onSaveRequestError(error);
                            }
                        })

                    }
                }
            });
        })

    </script>
@endsection
