<a href="{{ route('page.articleDetails', ['service' => $alias]) }}" class="btn btn-default" target="_blank"><i class="fa fa-fw fa-link"></i></a>
<a href="{{ route('articles.edit', $id) }}" class="btn btn-primary"><i class="fa fa-fw fa-pencil"></i></a>
<a data-fancybox data-type="ajax" data-src="{{ route('articles.delete', $id) }}" href="javascript:;" class="btn btn-danger"><i class="fa fa-fw fa-trash"></i></a>
