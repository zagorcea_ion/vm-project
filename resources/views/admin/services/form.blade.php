@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="panel-title">{{!empty($services->id) ? 'Editarea' : 'Adăuga'}} serviciu</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" action="{{!empty($services->id) ? route('services.update', ['service' => $services->id]) : route('services.store')}}" class="form-horizontal" id="services_form" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <?php if(!empty($services->id)){?>
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" id='id' name="id" value="{{$services->id}}">
                    <?php }?>

                    <div class="form-group">
                        <label for="title">Titlu</label>
                        <input type="text" class="form-control validate[required, maxSize[250]]" id="title" name="title" placeholder="Titlu:" value="{{!empty($services->title) ? $services->title : ''}}">
                    </div>

                    <div class="form-group">
                        <label for="description">Descriere</label>
                        <textarea class="form-control validate[required, maxSize[250]]" id="description" name="description" rows="4" cols="80" style="resize: none" placeholder="Descriere:">{{!empty($services->description) ? $services->description : ''}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="editor">Text</label>
                        <textarea class="validate[required]" id="editor" name="text" rows="10" cols="80">{{!empty($services->text) ? $services->text : ''}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Limba</label>
                        <select class="form-control validate[required]" name="lang">
                            @if(empty($services->lang))
                                <option value="" selected>Selecteaza limba</option>
                            @endif

                            <option value="ru" {{ (!empty($services->lang) && $services->lang == "ru") ? "selected" : "" }}>RU</option>
                            <option value="ro" {{ (!empty($services->lang) && $services->lang == "ro") ? "selected" : "" }}>RO</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Prioritate</label>
                        <input type="number" class="form-control" id="priority" name="priority" placeholder="Prioritate:" min="1" max="99" value="{{!empty($services->priority) ? $services->priority : ''}}">
                    </div>

                    <div class="form-group">
                        <label for="image">Imaginea</label>
                        <input type="file" id="image" name="image">
                    </div>

                    <div class="form-group">
                        @if(isset($services->image) && !empty($services->image))
                            <img src="/public/images/services/{{ $services->image }}" width="200px">
                            <input type="hidden" value="{{ $services->image }}" name="old_image">
                        @elseif(isset($services->id) && !empty($services->id))
                            <img src="/public/images/noImg.jpg" width="200px">
                        @endif
                    </div>


                    <button class="btn btn-default" type="reset">Curăța</button>
                    <button class="btn btn-primary pull-right" type="submit">Adăuga</button>

                </form>
            </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>

    <script>
        $( document ).ready(function() {
            tinymce.init({
                selector: '#editor',
                entity_encoding: 'raw',
                menubar: false,
                branding: false,
                height: 500,
                max_height: 700,
                min_height: 500,
                plugins: [
                    'autolink autoresize fullscreen link lists paste',
                ],
                toolbar: 'undo redo | bold italic | underline strikethrough | bullist numlist | link ',

                link_assume_external_targets: true,
                relative_urls: false,
                image_advtab: true ,
                remove_script_host: false,
                force_br_newlines: false,
                force_p_newlines: false,
                forced_root_block: "",
                extended_valid_elements: "br",
                verify_html: false,
                valid_children: "br",
                paste_as_text: true,

            });

            $("#services_form").validationEngine({
                promptPosition : "topLeft",
                onValidationComplete: function(form, status){
                    if(status){

                        $.ajax ({
                            url : $('#services_form').attr('action'),
                            type: 'POST',
                            data: new FormData($('#services_form')[0]),
                            dataType: 'JSON',
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false, // NEEDED, DON'T OMIT THIS
                            success: function (response) {
                                new Noty({type: 'success', layout: 'topRight', text: response.message, timeout:3000}).show();
                                document.location.href = '{{ route('services.index') }}';

                                if(dt) {
                                    dt.draw(false)
                                }
                            },
                            error: function (error) {
                                onSaveRequestError(error);
                            }
                        })

                    }
                }
            });

        });
    </script>

@endsection
    