<a href="{{ route('page.serviceDetails', ['service' => $alias]) }}" class="btn btn-default" target="_blank"><i class="fa fa-fw fa-link"></i></a>
<a href="{{ route('services.edit', $id) }}" class="btn btn-primary"><i class="fa fa-fw fa-pencil"></i></a>
<a data-fancybox data-type="ajax" data-src="{{ route('services.delete', $id) }}" href="javascript:;" class="btn btn-danger"><i class="fa fa-fw fa-trash"></i></a>
