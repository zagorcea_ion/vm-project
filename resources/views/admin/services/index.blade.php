@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Servicii</h3>
            <div class="box-tools">
                <a href="{{ route('services.create') }}" class="btn btn-block btn-primary "><i class="fa fa-fw fa-plus"></i> Adăuga serviciu</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="practice_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
                    <th>Imaginea</th>
                    <th>Titlu</th>
                    <th>Descrierea</th>
                    <th>Data</th>
                    <th>Limba</th>
                    <th>Prioritate</th>
                    <th>Acţiuni</th>
                </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>
    <script>

        var dt;
        $(document).ready(function() {
            dt = $('#practice_table').DataTable( {
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                responsive: true,
                //"searching": false,
                stateSave: true,
                "order": [ 3, 'desc' ],
                "language": {
                    "lengthMenu": "_MENU_ pe pagina",
                    "zeroRecords": "Nimic nu a fost găsit - îmi pare rău",
                    "info": "Pagina _PAGE_ din _PAGES_",
                    "infoEmpty": "Nu există înregistrări disponibile",
                    "infoFiltered": "(filtrarea a _MAX_ intrări)",
                    "sSearch": "Caută:",
                    "oPaginate": {
                        "sFirst":    "Prima",
                        "sLast":    "Ultima",
                        "sNext":    "Următoarea",
                        "sPrevious": "Precedenta"
                    }
                },
                "columnDefs": [
                    {orderable: false, className: "w-50", "targets": 0},
                    {orderable: true,  searchable: true, "targets": 1},
                    {orderable: false, className: "w-300", searchable: false, "targets": 2},
                    {orderable: true, className: "w-100", searchable: false, "targets": 3},
                    {orderable: false, className: "w-50", searchable: false, "targets": 4},
                    {orderable: true, className: "w-50", searchable: false, "targets": 5},
                    {orderable: false, className: "w-150", searchable: false, "targets": 6},

                ],
                "columns": [
                    {"data": "image"},
                    {"data": "title"},
                    {"data": "description"},
                    {"data": "date"},
                    {"data": "lang"},
                    {"data": "priority"},
                    {"data": "actions"},

                ],

                "ajax": {
                    url: "{{route('services.list')}}",
                    type: 'POST',
                }
            });
        });
    </script>
@endsection