@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Profil</h3>
            <div class="box-tools">
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-4 col-md-offset-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue">
                        <div class="widget-user-image">
                            @if(!empty($user['image']))
                                <img src="{{asset('images/users') . '/' . $user['image']}}" class="img-circle" alt="{{$user['surname']}} {{$user['name']}}" style="margin-top: -10px">
                            @else
                                <img src="{{asset('images/noavatar.png')}}" class="img-circle" alt="{{$user['surname']}} {{$user['name']}}" style="margin-top: -10px">
                            @endif

                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{{$user['surname']}} {{$user['name']}}</h3>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#"><i class="fa fa-fw fa-user"></i><strong> Login:</strong> {{$user['login']}}</a></li>
                            <li><a href="#"><i class="fa fa-fw fa-envelope"></i><strong> Email:</strong> {{$user['email']}}</a></li>

                        </ul><br>
                        <div class="row">
                            <div class="col-md-12">
                                <a data-fancybox data-type="ajax" data-src="{{ route('users.edit', $user['id']) }}" href="javascript:;" class="btn btn-primary" style="width: 100%; margin-top: 10px"><i class="fa fa-fw fa-pencil"></i> Editа</a>

                            </div>
                            <div class="col-md-12">
                                <a data-fancybox data-type="ajax" data-src="{{ route('users.password', $user['id']) }}" href="javascript:;" class="btn btn-default" style="width: 100%; margin-top: 10px"><i class="fa fa-fw fa-unlock-alt"></i> Schimba parola</a>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>

@endsection