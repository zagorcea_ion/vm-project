<a data-fancybox data-type="ajax" 
    data-src="{{ route('consultation.requests.details', $id) }}" 
    href="javascript:;" 
    class="btn btn-default" 
    title="Vezi detalii">
    <i class="fa fa-fw fa-eye"></i>
</a>

<a data-fancybox data-type="ajax" 
    data-src="{{ route('consultation.requests.delete', $id) }}" 
    href="javascript:;" 
    class="btn btn-danger" 
    title="Șterge înregistrare">
    <i class="fa fa-fw fa-trash"></i>
</a>
