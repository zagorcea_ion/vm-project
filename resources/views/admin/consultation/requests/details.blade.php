<div style="width:700px">

    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">Cererea №{{ $request->id }}</h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-12 invoice-col">
    
        <div><strong>Nume Prenume: </strong>{{ $request->surname . ' ' . $request->name }}</div>
        <div><strong>Telefon: </strong>{{ $request->phone }}</div>
        <div><strong>E-mail: </strong>{{ $request->email }}</div>
        <div><strong>Modul de comunicare: </strong>{{ $request->communication_type }}</div>
        <div><strong>Data: </strong>{{ date('d.m.Y', strtotime($request->created_at)) }}</div>
        <div>
            <strong>Achitat: </strong> 
            @if ($request->paid)
                <small class="label bg-green" style="width:30px">Da</small>
            @else
                <small class="label bg-red" style="width:30px">Nu</small>
            @endif
        </div>

        <div>
            <strong>Descrierea problemei:</strong>
            <p>{{ $request->subject }}</p>
        </div>

      </div>

      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <th>Serviciul juridic:</th>
            <th>Ore</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($services as $item)
                <tr>
                    @if (isset($consultationService[$item['service_id']]))
                        <td>{{ $consultationService[$item['service_id']]['title']}}</td>
                    @else
                        <td>Serviciul a fost șters</td>
                    @endif
                   
                    <td>{{ $item['hours'] }}</td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-xs-12 table-responsive">
            <div><strong>Suma totala: </strong>{{ $request->paid_sum }} €</div>
        </div>
    </div>
</div>