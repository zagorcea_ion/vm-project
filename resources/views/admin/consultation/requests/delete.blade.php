<div style="width:300px">

    <div class="col-md-12">

        <form method="POST" class="form-horizontal" id="request_delete" action="{{ route('consultation.requests.destroy', $id) }}">
            {!! csrf_field() !!}
            <input type="hidden" name="_method" value="DELETE">

            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title"><strong>Șterge cerere</strong></h3>
                <div class="panel-body">
                    <p>Ești sigur?</p>
                </div>
            </div>

            <button class="btn btn-default" id="cancel">Anulare</button>
            <button class="btn btn-danger pull-right" type="submit">Da</button>
        </form>
    </div>
</div>


<script>

    var url = $('#request_delete').attr('action');

    $('#cancel').on('click', function(){
        $.fancybox.close();
        return false;
    });

    $("#request_delete").submit(function(e){
        e.preventDefault();

        $.post(url, $('#request_delete').serialize(), null, 'json').done(function(response) {
            new Noty({type: 'success', layout: 'topRight', text: response.message, timeout:3000}).show();

            $.fancybox.close();

            if(dt) {
                dt.draw(false)
            }
        }).fail(onSaveRequestError);

    });
</script>
