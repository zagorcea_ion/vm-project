@extends('layouts.admin')

@section('content')
@include('admin.components.success-message')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">CONSULTANȚĂ Cereri</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="blog_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
                    <th>Nume Prenume</th>
                    <th>Telefon</th>
                    <th>E-mail</th>
                    <th>Descrierea problemei</th>
                    <th>Achitat</th>
                    <th>Data</th>
                    <th>Acţiuni</th>
                </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>
    <script>

        var dt;
        $(document).ready(function() {
            dt = $('#blog_table').DataTable( {
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                responsive: true,
                stateSave: true,
                "order": [ 5, 'desc' ],
                "language": {
                    "lengthMenu": "_MENU_ pe pagina",
                    "zeroRecords": "Nimic nu a fost găsit - îmi pare rău",
                    "info": "Pagina _PAGE_ din _PAGES_",
                    "infoEmpty": "Nu există înregistrări disponibile",
                    "infoFiltered": "(filtrarea a _MAX_ intrări)",
                    "sSearch": "Caută:",
                    "oPaginate": {
                        "sFirst":    "Prima",
                        "sLast":    "Ultima",
                        "sNext":    "Următoarea",
                        "sPrevious": "Precedenta"
                    }
                },
                "columnDefs": [
                    {orderable: true, className: "w-100", searchable: true, "targets": 0},
                    {orderable: false, className: "w-100", searchable: true, "targets": 1},
                    {orderable: false, className: "w-100", searchable: true, "targets": 2},
                    {orderable: false, searchable: true, "targets": 3},
                    {orderable: false, className: "w-50", searchable: false, "targets": 4},
                    {orderable: true, className: "w-50", searchable: false, "targets": 5},
                    {orderable: false, className: "w-150", searchable: false, "targets": 6},

                ],
                "columns": [
                    {"data": "full_name"},
                    {"data": "phone"},
                    {"data": "email"},
                    {"data": "subject"},
                    {"data": "paid"},
                    {"data": "date"},
                    {"data": "actions"},

                ],

                "ajax": {
                    url: "{{route('consultation.requests.list')}}",
                    type: 'POST',
                }
            });
        });
    </script>
@endsection