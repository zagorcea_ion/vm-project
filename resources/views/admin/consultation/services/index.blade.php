@extends('layouts.admin')

@section('content')
@include('admin.components.success-message')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">CONSULTANȚĂ Servicii</h3>
            <div class="box-tools">
                <a href="{{ route('consultation.services.create') }}" class="btn btn-block btn-primary "><i class="fa fa-fw fa-plus"></i> Adăuga serviciu</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="blog_table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
                    <th>Titlu</th>
                    <th>Preț pe ora</th>
                    <th>Prioritate</th>
                    <th>Data</th>
                    <th>Acţiuni</th>
                </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>
    <script>

        var dt;
        $(document).ready(function() {
            dt = $('#blog_table').DataTable( {
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                responsive: true,
                stateSave: true,
                "order": [ 3, 'desc' ],
                "language": {
                    "lengthMenu": "_MENU_ pe pagina",
                    "zeroRecords": "Nimic nu a fost găsit - îmi pare rău",
                    "info": "Pagina _PAGE_ din _PAGES_",
                    "infoEmpty": "Nu există înregistrări disponibile",
                    "infoFiltered": "(filtrarea a _MAX_ intrări)",
                    "sSearch": "Caută:",
                    "oPaginate": {
                        "sFirst":    "Prima",
                        "sLast":    "Ultima",
                        "sNext":    "Următoarea",
                        "sPrevious": "Precedenta"
                    }
                },
                "columnDefs": [
                    {orderable: true,  searchable: true, "targets": 0},
                    {orderable: false, searchable: true, className: "w-100", searchable: false, "targets": 1},
                    {orderable: true,  searchable: true, className: "w-100", searchable: false, "targets": 2},
                    {orderable: false, searchable: true, className: "w-50", searchable: false, "targets": 3},
                    {orderable: false, className: "w-150", searchable: false, "targets": 4},

                ],
                "columns": [
                    {"data": "title"},
                    {"data": "price"},
                    {"data": "priority"},
                    {"data": "date"},
                    {"data": "actions"},

                ],

                "ajax": {
                    url: "{{route('consultation.services.list')}}",
                    type: 'POST',
                }
            });
        });
    </script>
@endsection