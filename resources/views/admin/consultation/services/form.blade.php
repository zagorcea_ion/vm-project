@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="panel-title">{{ !empty($service->id) ? 'Editarea' : 'Adăuga' }} serviciu</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-12">
                <form method="POST" action="{{ !empty($service->id) ? route('consultation.services.update', ['service' => $service->id]) : route('consultation.services.store') }}" class="form-horizontal" id="service-form">
                    {!! csrf_field() !!}

                    @if(!empty($service->id))
                        <input type="hidden" name="_method" value="PUT">
                    @endif

                    <div class="form-group">
                        <label for="title">Titlu</label>
                        <input type="text" class="form-control validate[required, maxSize[250]]" id="title" name="title" placeholder="Title:" value="{{ !empty($service->title) ? $service->title : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="title">Preț pe ora</label>
                        <input type="number" class="form-control validate[required, onlyNumber]" min="0" id="price" name="price" placeholder="Preț pe ora:" value="{{ !empty($service->price) ? $service->price : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="priority">Prioritate</label>
                        <input type="number" class="form-control" min="0" id="priority" name="priority" placeholder="Prioritate:" value="{{ !empty($service->priority) ? $service->priority : '' }}">
                    </div>

                    <button class="btn btn-default" type="reset">Curăța</button>
                    <button class="btn btn-primary pull-right" type="submit">Adăuga</button>

                </form>
            </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-body -->
    </div>

    <script>
        $( document ).ready(function() {

            $("#service-form").validationEngine({
                promptPosition : "topLeft",
                onValidationComplete: function(form, status){
                    if(status){

                        $.ajax ({
                            url : $('#service-form').attr('action'),
                            type: 'POST',
                            data: new FormData($('#service-form')[0]),
                            dataType: 'JSON',
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false, // NEEDED, DON'T OMIT THIS
                            success: function (response) {
                                // new Noty({type: 'success', layout: 'topRight', text: response.message, timeout:3000}).show();
                                document.location.href = '{{ route('consultation.services.index') }}';

                            },
                            error: function (error) {
                                onSaveRequestError(error);
                            }
                        })

                    }
                }
            });
        })

    </script>
@endsection
