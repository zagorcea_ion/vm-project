<div style="width:300px">

    <div class="col-md-12">

        <form method="POST" class="form-horizontal" id="service_delete" action="{{ route('consultation.services.destroy', $id) }}">
            {!! csrf_field() !!}
            <input type="hidden" name="_method" value="DELETE">

            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title"><strong>Șterge serviciul</strong></h3>
                <div class="panel-body">
                    <p>Ești sigur?</p>
                </div>
            </div>

            <button class="btn btn-default" id="cancel">Anulare</button>
            <button class="btn btn-danger pull-right" type="submit">Da</button>
        </form>
    </div>
</div>


<script>

    var url = $('#service_delete').attr('action');

    $('#cancel').on('click', function(){
        $.fancybox.close();
        return false;
    });

    $("#service_delete").submit(function(e){
        e.preventDefault();

        $.post(url, $('#service_delete').serialize(), null, 'json').done(function(response) {
            new Noty({type: 'success', layout: 'topRight', text: response.message, timeout:3000}).show();

            $.fancybox.close();

            if(dt) {
                dt.draw(false)
            }
        }).fail(onSaveRequestError);

    });
</script>
