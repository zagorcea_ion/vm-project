<a href="{{ route('consultation.services.edit', $id) }}" class="btn btn-primary"><i class="fa fa-fw fa-pencil"></i></a>
<a data-fancybox data-type="ajax" data-src="{{ route('consultation.services.delete', $id) }}" href="javascript:;" class="btn btn-danger"><i class="fa fa-fw fa-trash"></i></a>
