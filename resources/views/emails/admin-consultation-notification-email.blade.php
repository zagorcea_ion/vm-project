<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8" />
</head>
<body>

    <table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica"> 
        <tbody>
            <tr width="100%"> 
                <td valign="top" align="left" style="background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica"> 
                    <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> 
                        <tbody> 
                            <tr width="100%" height="60"> 
                                <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background-color:#ffffff;padding:10px 18px">
                                    <img height="40" width="200" src="http://lawoffice.md/images/logo.png" title="Trello" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> 
                                </td> 
                            </tr> 
                            <tr width="100%"> 
                                <td valign="top" align="left" style="background:#fff;padding:18px">

                                    <h1 style="font-size:20px;margin:16px 0;color:#333;text-align:center"> Сerere de consultare </h1>
       
                                    <p style="font:14px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#333"> 
                                        Bună ziua, aveți o nouă solicitare de consultare. Pentru a vedea detaliile, accesați panoul de administrare
                                    </p>
                                </td>
                            </tr>
                        </tbody> 
                    </table> 
                </td> 
            </tr>
        </tbody> 
    </table>
</body>
</html>