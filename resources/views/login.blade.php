@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>Logare</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
        
    <!--inner-content start-->
    <div class="inner-content innerbg">
        <div class="container"> 
            <!-- Login start -->
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="login">
                        <div class="contctxt">Logare <span>utilizator</span></div>
                        <div class="formint conForm">
                            <form id="login_form">
                                <div class="input-wrap">
                                    <label class="input-group-addon">Login</label>
                                    <input type="text" name="login" placeholder="Login" class="form-control">
                                </div>
                                
                                <div class="input-wrap">
                                    <label class="input-group-addon">Parola</label>
                                    <input type="password" name="password" placeholder="Parola" class="form-control">
                                </div>

                                <div class="input-wrap">
                                    <input type="checkbox" id="remember" name="remember">
                                    <label for="remember" id="remmeber_lable">Tine minte</label>
                                </div>

                                <div class="sub-btn">
                                    <input type="submit" class="sbutn" value="Logare">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <!-- Login end --> 
        </div>
    </div>
    <!--inner-content end--> 
    <script>
        $( "#login_form" ).submit(function(e) {
            e.preventDefault();
    
            $.ajax({
                url : '{{route('authenticate')}}',
                type : 'POST',
                dataType : 'JSON',
                data : $('#login_form').serialize(),
                success: function (response) {
                    window.location="{{route('articles.index')}}";
    
                },
                error: function (error) {
                    onSaveRequestError(error);
                }
            });
        });
    
    </script>
@endsection