@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
          <h3>{{__('words.nav_services')}}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 
          
            <!-- blog start -->
            <div class="blogWraper blogdetail">
                <div class="blogList">

                    <div class="postimg">
                        @if(!empty($service->image) && file_exists(public_path().'/images/services/' . $service->image))
                            <img src="{{asset('images/services/' . $service->image)}}" alt="{{$service->title}}">
                        @else
                            <img src="{{asset('images/noImg.jpg')}}" alt="{{$service->title}}">
                        @endif
                    </div>
                    <div class="post-header margin-top30">
                        <h4>{{$service->title}}</h4>
                    </div>
                    {!! $service->text !!}

                </div>
            </div>
            <!-- blog end -->

        </div>
    </div>
    <!--inner-content end--> 
@endsection