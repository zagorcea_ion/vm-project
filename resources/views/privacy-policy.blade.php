@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>{{ __('words.nav_privacy_policy') }}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 
            <div class="row">
                <div class="col-md-12">
                    <ol class="big-list-alpha">
                        <li><h2>Introducere</h2></li>
                        
                        <ol class="decimal-list">
                            <li><p>Confidențialitatea vizitatorilor website-ului nostru este foarte importantă pentru noi și
                                ne luăm angajamentul să o protejăm. Această politică explică ce vom face cu informațiile
                                dumneavoastră personale.</p>
                            </li>
                            <li>
                                <p>Acordarea consimțământului asupra utilizării cookie-urilor în concordanță cu termenii
                                    acestei politici atunci când vizitați website-ul nostru pentru prima dată, ne permite să
                                    utilizăm cookie-uri de fiecare dată când vizitați website-ul nostru.</p>
                            </li>
                        </ol>

                        <li><h2>Colectarea informațiilor personale</h2></li>

                        <p>Pot fi colectate, stocate și utilizate următoarele tipuri de informații personale:</p>

                        <ol class="decimal-list">
                            
                            <li>
                                <p>informații precum adresa dvs. de e-mail, pe care le introduceți atunci când vă
                                    înregistrați pe website-ul nostru;</p>
                            </li> 
                            <li>
                                <p>informațiile pe care le introduceți atunci când plasați o comandă pe website-ul nostru –
                                    de exemplu, numele dvs.</p>
                            </li> 
                            <li>
                                <p>informații precum numele și adresa dvs. de e-mail, pe care le introduceți pentru a
                                    configura abonările la e-mailurile.</p>
                            </li> 
                            <li>
                                <p>informații pe care le introduceți în timp ce utilizați serviciile de pe website-ul nostru;</p>
                            </li> 
                            <li>
                                <p>informații care sunt generate în timp ce utilizați website-ul nostru, inclusiv despre când,
                                    cât de des și în ce circumstanțe îl utilizați;</p>
                            </li>
                            <li>
                                <p>informații referitoare la orice achiziție efectuată, servicii utilizate sau tranzacții pe care
                                    le faceți prin intermediul website-ului nostru, care pot include numele, adresa, numărul
                                    de telefon, adresa de e-mail și detaliile cardului bancar;</p>
                            </li>   
                            <li>
                                <p>informații conținute în orice comunicări ne trimiteți prin e-mail sau prin intermediul
                                    website-ului nostru, inclusiv conținutul comunicărilor și metadatele acestora;</p>
                            </li> 
                            <li>
                                <p>orice alte informații personale pe care ni le trimiteți.</p>
                            </li>
                        </ol>
                        <p>Înainte de a ne divulga informațiile personale ale unei alte persoane, trebuie să obțineți
                            consimțământul acelei persoanei atât pentru divulgarea, cât și pentru procesarea informațiilor
                            personale în conformitate cu această politică.</p>

                        <li><h2>Utilizarea informațiilor dvs. Personale</h2></li>

                        <p>Informațiile personale transmise prin intermediul website-ului nostru vor fi utilizate în scopurile specificate de această politică sau în paginile respective ale website-ului.
                            Putem folosi informațiile dvs. personale pentru:</p>

                        <ol class="decimal-list">
                            <li>
                                <p>autorizarea utilizării serviciilor disponibile pe website-ul nostru;</p>
                            </li>
                            <li>
                                <p>furnizarea serviciilor juridice prin intermediul website-ului nostru;</p>
                            </li>
                            <li>
                                <p>trimiterea de notificări de plată către dvs. și colectarea plăților de la dvs;</p>
                            </li>
                            <li>
                                <p>trimiterea de comunicări comerciale în vederea informării;</p>
                            </li>
                            <li>
                                <p>trimiterea prin e-mail de notificări solicitate în mod expres;</p>
                            </li>
                            <li>
                                <p>furnizarea către terți a informațiilor statistice despre utilizatorii noștri (acești terți nu vor putea identifica niciun utilizator cu ajutorul acestor informații);</p>
                            </li>
                            <li>
                                <p>abordarea solicitărilor și reclamațiilor făcute de dvs. sau despre dvs. referitoare la website-ul nostru;</p>
                            </li>
                            <li>
                                <p>păstrarea securității website-ului nostru și prevenirea fraudelor;</p>
                            </li>
                            <li>
                                <p>verificarea respectării termenilor și condițiilor care reglementează utilizarea website-ului nostru (inclusiv monitorizarea mesajelor private trimise prin intermediul
                                    serviciului nostru de mesagerie privată); și alte utilizări.</p>
                            </li>
                        </ol>

                        <li><h2>Divulgarea informațiilor personale</h2></li>
                        <p>Informațiile dvs. personale nu vor fi furnizate către terți.</p>

                        <li><h2>Păstrarea informațiilor personale</h2></li>

                        <ol class="decimal-list">
                            <li>
                                <p>Această Secțiune E stabilește politicile și procedurile noastre de păstrare a datelor, care sunt concepute pentru a ajuta la asigurarea respectării
                                    obligațiilor noastre legale cu privire la păstrarea și ștergerea informațiilor personale.</p>
                            </li>
                            <li>
                                <p>Informațiile personale pe care le prelucrăm cu orice scop sau scopuri nu vor fi păstrate mai mult decât este necesar pentru acel scop sau scopuri.</p>
                            </li>
                            <li>
                                <p>Fără a aduce atingere articolului E-2, de obicei ștergem datele cu caracter personal care se încadrează în categoriile prezentate mai jos la data/ora stabilite mai jos:</p>
                            </li>
                                <ol class="decimal-list">
                                    <li><p>informațiile de tip personal vor fi șterse {INTRODUCEȚI DATA/ORA}; și</p></li>
                                    <li><p>{INTRODUCEȚI MAI MULTE DATE/ORE}.</p></li>
                                </ol>
                            <li>
                                <p>În pofida celorlalte dispoziții din această Secțiune E, vom păstra documente (inclusiv documente electronice) care conțin date cu caracter personal:</p>
                            </li>
                                <ol class="decimal-list">
                                    <li><p>în măsura în care ni se solicită acest lucru prin lege;</p></li>
                                    <li><p>dacă considerăm că documentele pot fi relevante pentru orice procedură legală în derulare sau viitoare</p></li>
                                </ol>
                        </ol>

                        <li><h2>Securitatea informațiilor dvs. personale</h2></li>
                        <ol class="decimal-list">
                            <li>
                                <p>Vom lua măsuri de precauție tehnice și organizaționale rezonabile pentru a preveni pierderea, utilizarea necorespunzătoare sau modificarea informațiilor dvs. personale.</p>
                            </li>
                            <li>
                                <p>Vom stoca toate informațiile personale pe care le oferiți pe serverele noastre securizate (protejate prin parolă și firewall).</p>
                            </li>
                            <li>
                                <p>Toate tranzacțiile financiare electronice încheiate prin intermediul website-ului nostru vor fi protejate de tehnologia de criptare.</p>
                            </li>
                            <li>
                                <p>Ați luat la cunoștință faptul că transmiterea informațiilor pe internet este în mod obișnuit nesigură și nu putem garanta securitatea datelor trimise pe internet.</p>
                            </li>
                            <li>
                                <p>Sunteți responsabil(ă) pentru păstrarea confidențialității parolei pe care o utilizați pentru accesarea website-ului nostru; nu vă vom solicita niciodată parola
                                    (cu excepția momentului când vă conectați pe website-ul nostru).</p>
                            </li>
                        </ol>

                        <li><h2>Modificări</h2></li>
                        <p>Ne rezervăm dreptul de a actualiza această politică din când în când publicând o versiune nouă pe website-ul nostru. Trebuie să verificați ocazional această pagină pentru a
                            vă asigura că înțelegeți orice modificare adusă acestei politici. Vă putem anunța despre modificările aduse acestei politici prin e-mail sau prin sistemul de mesagerie privată
                            de pe website-ul nostru.
                        </p>

                        <li><h2>Drepturile dvs.</h2></li>
                        <p>Ne puteți solicita să vă oferim orice informații personale deținem despre dvs.; furnizarea acestor informații va fi supusă următorilor termeni:</p>

                        <ol class="decimal-list">
                            <li>
                                <p>plata unei taxe {INTRODUCEȚI „GRATUIT” DACĂ SE APLICĂ}; și</p>
                            </li>
                            <li>
                                <p>furnizarea de dovezi suficiente cu privire la identitatea dvs. ({MODIFICAȚI TEXTUL PENTRU A REFLECTA POLITICA DVS. în acest scop, de regulă acceptăm o
                                    fotocopie legalizată la notar a pașaportului dvs, plus o copie certificată „conform cu originalul” a unei facturi de utilități care conține adresa dvs. curentă}).
                                </p>
                            </li>
                        </ol>

                        <p>Putem reține informațiile personale pe care le solicitați în măsura permisă de lege.
                            Ne puteți solicita în orice moment să nu prelucrăm informațiile dvs. personale în alte scopuri decât cele solicitate.
                            În practică, de obicei fie vă exprimați acordul expres prealabil cu privire la utilizarea informațiilor dvs. personale.
                        </p>

                        <li><h2>Site-uri terțe</h2></li>
                        <p>Site-ul nostru include hyperlink-uri către și detalii despre site-uri terțe. Nu deținem niciun control și nu de facem responsabili pentru politicile și practicile de confidențialitate ale terților.</p>

                        <li><h2>Actualizarea informațiilor</h2></li>
                        <p>Vă rugăm să ne anunțați dacă informațiile personale pe care le deținem despre dvs. trebuie să fie corectate sau actualizate.</p>

                    </ol>
                </div>
            </div>
            
        </div>
    </div>
@endsection 