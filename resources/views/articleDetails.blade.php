@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
          <h3>{{__('words.nav_articles')}}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 

            <!-- blog start -->
            <div class="blogWraper blogdetail">
                <div class="blogList">
                    @if (!empty($article->youtube_video))
                        <div class="youtube-video">
                            <iframe src="https://www.youtube.com/embed/{{ $article->youtube_video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    @else
                        <div class="postimg">
                            @if(!empty($article->image) && file_exists(public_path() . '/images/articles/' . $article->image))
                                <img src="{{asset('images/articles/' . $article->image)}}" alt="{{$article->title}}">
                            @else
                                <img src="{{asset('images/noImg.jpg')}}" alt="{{$article->title}}">
                            @endif
                            <div class="date">{{ Carbon\Carbon::parse($article->created_at)->format('d.m.Y') }}</div>
                        </div>
                    @endif


                    <div class="post-header margin-top30">
                        <h4>{{$article->title}}</h4>
                    </div>

                    {!! $article->text !!}

                    <div class="row social-share-wrapper">
                        <div class="col-md-12">
                            <h4 class="mt-2 pt-4">Distribuie acest articol, alege platforma ta</h4>

                            <div class="d-sm-flex">
                                <div class="social-share-block facebook">
                                    <a title="Distribuie pe facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ route('page.articleDetails', ['article'=>$article->alias]) }}">
                                        <div class="social-share-icon-block">
                                            <img src="{{ asset('images/social-svg/social-facebook.svg') }}" title="Distribuie pe facebook" alt="Distribuie pe facebook">
                                            <span class="share-text">Facebook</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="social-share-block twitter ">
                                    <a title="Distribuie pe twitter" target="_blank" href="https://twitter.com/home?status={{ route('page.articleDetails', ['article'=>$article->alias]) }}">
                                        <div class="social-share-icon-block">
                                            <img src="{{ asset('images/social-svg/social-twitter.svg') }}" alt="Distribuie pe twitter" title="Distribuie pe twitter">
                                            <span class="share-text">Twitter</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="social-share-block linkedin">
                                    <a title="Distribuie pe linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('page.articleDetails', ['article'=>$article->alias]) }}">
                                        <div class="social-share-icon-block">
                                            <img src="{{ asset('images/social-svg/social-linkedin.svg') }}" alt="Distribuie pe linkedin" title="Distribuie pe linkedin">
                                            <span class="share-text">Linkedin</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="social-share-block ok d-block d-sm-none">
                                    <a title="Distribuie pe odnoklassniki" target="_blank" href="https://connect.ok.ru/offer?url={{ route('page.articleDetails', ['article'=>$article->alias]) }}">
                                        <div class="social-share-icon-block">
                                            <img src="{{ asset('images/social-svg/social-ok.svg') }}" alt="Distribuie pe odnoklassniki" title="Distribuie pe odnoklassniki">
                                            <span class="share-text">Ok</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="wrapper-fb-comments">
                            <h4 class="mt-2 pt-4">Lasă comentariul tău</h4>
                            <div class="fb-comments" data-href="{{ route('page.articleDetails', ['article'=>$article->alias]) }}" data-numposts="5" data-width="100%"></div>

                        </div>


                </div>
            </div>
            <!-- blog end -->
        </div>
    </div>
    <!--inner-content end--> 
@endsection