@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->  
    <div class="inner-heading">
        <div class="container">
          <h3>{{__('words.nav_about')}}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 
          
            <!-- about-wrap start -->
            <div class="welcomeWrap">
                <div class="row">
                    <div class="col-md-4">
                    <div class="welImg"><img src="{{asset('images/welcome-img.jpg')}}" alt=""></div>
                    </div>
                    <div class="col-md-8">
                        <h1>{!! __('words.about_title') !!}</h1>
                        <p>{!! __('words.about_description') !!}</p>
                    </div>
                </div>
            </div>
            <!-- about-wrap end -->   
        </div>
        
    </div>
    <!--inner-content end--> 
      

@endsection