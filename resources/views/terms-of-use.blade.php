@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>{{ __('words.nav_terms_of_use') }}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 info-pages">
                    <ol class="big-list-decimal">
                        <li><h2>Client</h2></li>
                        <p>Orice persoana fizica sau persoana juridica, interesată de serviciile juridice prestate de catre avocat, care efectueaza o Comanda avand ca obiect achizitionarea de
                            servicii juridice de Consultanta Online.
                        </p>

                        <li><h2>Avocat</h2></li>
                        <p>Profesia de avocat este exercitată de persoane calificate şi abilitate, conform legii, să pledeze şi să acţioneze în numele clienţilor lor, să practice dreptul, să apare în faţa
                            unei instanţe judecătoreşti sau să consulte şi să reprezinte în materie juridică clienţii lor. Profesia de avocat este liberă şi independentă, cu organizare şi funcţionare
                            autonomă, în condiţiile legii şi ale statutului profesiei de avocat. Activitatea avocatului nu este activitate de întreprinzător.
                        </p>

                        <li><h2>Comanda</h2></li>
                        <p>Un formular electronic pus la dispozitie pe platforma lawoffice pe care Clientul il completeaza si il transmite catre avocat in scopul achizitionarii de servicii juridice de Consultanta Online. </p>

                        <li><h2>Onorariu</h2></li>
                        <p>Onorariul reprezinta suma indicată în tabel, reprezentând serviciile de Consultanță Online prestate intr-un interval de timp stabilit în același tabel.</p>

                        <li><h2>Consultanța juridica online</h2></li>
                        <p>Servicii juridice prestate prin intermediul Consultantei Online, video sau audio, cu ajutorul aplicațiilor Viber, WhatsApp, Telegram, telefon sau alte mijloace de comunicare la
                            distanță care permit o comunicare eficienta si in timp real intre avocat si client.
                        </p>

                        <li><h2>Durata unei consultante online</h2></li>
                        <p>Activitatea de consultanță juridică are o durata de 60 de minute. Consultanta online incepe odata cu momentul contactării clientului. Respectiv, odată ce comanda a fost plasată,
                            clientul nu mai poate retrage Comanda si nu mai poate solicita restituirea onorariului.
                        </p>

                        <li><h2>Servicii juridice</h2></li>
                        <p>Serviciile juridice prestate sunt din domenii precum: drept administrativ, contravenții,dreptul familiei, drept penal, drept fiscal, protectia consumatorului, violența în familie,
                            accidente rutiere etc.
                        </p>
                        <p>Potrivit Legii nr. 1260 din 19.07.2002 cu privire la avocatură, În exercitarea profesiei sale, avocatul este independent şi se supune numai legii, statutului profesiei de avocat şi
                            Codului deontologic al avocatului. Avocatul este liber în alegerea poziţiei sale şi nu este obligat să coordoneze această poziţie cu nimeni, în afară de client.
                        </p>

                        <li><h2>Modalitatea de efectuare a Comezii</h2></li>
                        <p><strong>Pentru a obține consultanța juridică online, se efectuează o Comanda pe site-ul lawoffice.md, parcurgând urmatorii pasi:</strong></p>

                        <p><strong>Pasul 1</strong></p>
                        <p>Daca sunteți de acord cu prețul afișat al consultanței online și celelalte servicii, completați formularul de pe site cu nume, prenume, numar de telefon si adresa de e-mail,
                            modalitatea de comunicare (viber, whatsapp, telegram sau telefon), folosind o scurta descriere a problemei juridice, punând intrebari ori solicitând anumite opinii juridice.
                            De asemenea, trebuie sa citiți termenii si condițiile, precum si politica de confidențialitate, iar ulterior sa bifați casuța corespunzatoare prin care vă dați acordul cu
                            acest conținut pentru a trece la pasul urmator.
                        </p>

                        <p><strong>Pasul 2</strong></p>
                        <p>Dupa completarea integrală a formularului, veți fi redirecționat către următoarea pagina pentru efectuarea plății. La fel, documentele pot fi trimise prin e-mail info@lawoffice.md.</p>

                        <p><strong>Pasul 3</strong></p>
                        <p>Veți primi un e-mail de confirmare a Consultantei Online cu informațiile si datele completate in formular, prin care avocatul confirmă angajamentul prestării serviciului juridic online.</p>

                        <p><strong>Pasul 4</strong></p>
                        <p>În scurt timp avocatul vă va contacta, prin modalitatea aleasă, pentru consultanța efectivă, care se va desfășura in intervalul pentru care s-a optat. </p>

                        <li><h2>Plata onorariului</h2></li>
                        <p>Toate serviciile juridice sunt contra cost si nu se ofera servicii juridice gratuite, de niciun fel. Daca doriti consultanță juridică gratuită, va recomandăm să apelati la avocații
                            puși la dispoziție de către CNAJGS
                        </p>
                        <p>Plata onorariului se efectueaza in lei, euro sau dolari, conform echivalentului în euro, cu cardul bancar, prin intermediul E-commerce MAIB la momentul completarii formularului de pe site.</p>
                        <p>Plata se face integral înainte de prestarea serviciilor juridice de consultanță online.</p>
                        <p>Plata este completă și validată odată cu primirea e-mail-ului de confirmare a Comenzii.</p>
                        <p>Onorariile sunt afișate pe site și acestea nu se vor modifica după plasarea comenzii.</p>

                        <li><h2>Restituirea platii onorariului</h2></li>
                        <p><strong>Restituirea plății onorariului are loc atunci cand:</strong></p>
                        <ul class="unordered-list">
                            <li>
                                <p>
                                    Consultanța juridică nu a fost prestată de avocat, din culpa sa, caz in care Clientul este notificat înainte de ora stabilita pentru inițierea consultanței online,
                                    acesta având și posibilitatea de a conveni cu avocatul amânarea consultanței online pentru o dată ulterioară, în termen de 45 de zile;
                                </p>
                            </li>
                            <li>
                                <p>
                                    Avocatul își rezervă dreptul de a refuza prestarea serviciilor în scopuri ilicite si imorale. În cazul in care Avocatul consideră că solicitarea serviciilor juridice poate
                                    avea un scop imoral sau ilicit, acesta îl va informa pe Client despre refuzul de a presta serviciile solicitate.
                                </p>
                            </li>
                        </ul>
                        <p>
                            În toate aceste situații, suma achitată cu titlu de onorariu pentru consultanta se va restitui integral, in termen de 48 de ore de la formularea cererii de restituire, de către
                            Client. Cu toate acestea, cu acordul Clientului, unde este cazul, se va putea stabili o noua Consultanta Online, in functie de disponibilitatea părților.
                        </p>

                        <li><h2>Anularea unei Comenzi</h2></li>
                        <p><strong>Avocatul poate anula o Comandă in următoarele situații:</strong></p>

                        <ul class="unordered-list">
                            <li><p>Tranzactia nu a fost validata de către MAIB;</p></li>
                            <li><p>Tranzacția nu a fost acceptată de către banca emitentă a cardului Clientului;</p></li>
                            <li><p>Clientul nu a efectuat integral Comanda, aceasta nefiind complet si corect accesată;</p></li>
                            <li><p>Avocatul apreciază că prin accesarea platformei online si plasarea Comenzii, Clientul urmărește un scop illicit, imoral sau poate cauza orice fel de prejudicii catre avocat;</p></li>
                            <li><p>Termenii si condițile prezente nu sunt respectate;</p></li>
                            <li>
                                <p>
                                    Consultanța online nu poate fi onorată din motive independente de voința avocatului, cum ar fi evenimentele de forță majoră, epidemii/pandemii, incendii, explozii,
                                    razboaie, acte de terorism, etc.
                                </p>
                            </li>
                        </ul>
                        <p>
                            <strong>In acest caz, avocatul </strong>va inștiința de îndată Clienții cu privire la anularea Comenzii, iar părțile vor fi repuse in situația anterioară plasării Comenzii,
                            prin restituirea către Client a onorariului achitat.
                        </p>

                        <li><h2>Prestarea serviciilor juridice de Consultanta Online</h2></li>
                        <p>Clientul va beneficia de consultanța juridică online, pentru întreaga perioadă de timp pentru care a achitat onorariul. </p>

                        <p>Pe parcursul consultanței juridice,avocatul va fi la dispoziția Clientului si va oferi informații de natură juridică, astfel: </p>

                        <ul class="unordered-list">
                            <li><p>se oferă raspunsuri la întrebările adresate avocaților, atât prin intermediul formularului de Comandă, cât si a celor adresate pe durata Consultantei online;</p></li>
                            <li><p>se emit opinii legale, solicitate în legatură cu problema comunicată de client;</p></li>
                            <li><p>se formulează recomandări;</p></li>
                            <li><p>se oferă soluții date de avocat, privind soluțiile la problemele sesizate.</p></li>
                        </ul>

                        <p>
                            Avocatul își rezervă <strong>dreptul de a nu răspunde la solicitările care nu sunt complete</strong> și/sau care nu conțin suficiente elemente de natura, care să contureze cazuistica sesizată,
                            la intrebarile formulate incorect sau neinteligibil.
                        </p>

                        <p>
                            Daca se <strong>intrerupe</strong> o Consultanță Online, din motive tehnice independente de voința părtilor și nu poate fi reluată comunicarea in 10 minute, clientul are dreptul de a obține
                            reprogramarea consultanței online, in aceleași condiții, pentru restul de timp rămas neconsumat, la același avocat, in termeni restrânși.
                        </p>

                        <p><strong>Daca Clientul raspunde la apelul avocatului pentru inițierea Consultanței Online,</strong> consultanța se prelungește automat cu minutele corespunzatoare intârzierii. </p>

                        <p>
                            De asemenea, avocatul iși rezervă <strong>dreptul de a nu răspunde problemelor noi</strong> sesizate pe durata Consultanței Online, care necesită un studiu aprofundat si la care nu se poate
                            raspunde in termenul solicitat de client.
                        </p>

                        <p>
                            <strong>Dacă o consultanță online dureaza mai putin</strong> decât durata pentru care Clientul a plătit și aceasta se incheie cu acordul Clientului, nu se restituie nicio suma Clientului ca
                            reprezentând valoarea timpului de consultanță neprestata.Onorariul nu este divizibil.
                        </p>

                        <p>
                            <strong>Daca o consultanță online depașește durata achitată de către client</strong>, cu acordul clientului, caruia i se va comunica onorariul suplimentar și ân masura disponibilitații timpului
                            avocatului, se va continua consultanța si se va achita suplimentar.
                        </p>

                        <p>
                            Avocatul iși rezervă <strong>dreptul de a inceta consultanța începută sau de a refuza o consultanță neincepută</strong>, în situația în care consultanța online nu se desfășoară intr-un cadru
                            civilizat, Clientul arată un comportament contrar bunelor moravuri, o comunicare necorespunzătoare, prin care aduce ofense ori injurii avocatului, clientul neavând dreptul să
                            ceară restituirea onorariului achitat.
                        </p>

                        <p>
                            Raspunsurile oferite de avocat trebuie sa fie pe întelesul clientului, astfel incât să poată întelege din punct de vedere juridic problema sesizată, cât si raspunsurile ori
                            recomandarile avocatului. Avocatul poate face referințe la acte normative si jurisprudentiale, întotdeauna inteligibile, chiar si de către cei care nu sunt tehnicieni juridici,
                            atenți să sugereze soluții practice si strategii concrete de adoptat pentru soluționarea cazului propus.
                        </p>

                        <p>
                            Materialele trimise, precum si orice date sensibile transmise prin intermediul e-mailului <a href="mailto:info@lawoffice.md">info@lawoffice.md</a>, sunt confidențiale, reglementate de secretul profesional și vor
                            fi tratate in conformitate cu legislatia de confidențialitate.
                        </p>

                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection 