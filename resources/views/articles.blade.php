@extends('layouts.main')
@section('content')

    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>{{__('words.nav_articles')}}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 
            <!-- blog start -->
            <div class="blogWraper">
                <ul class="row blogGrid">
                    @foreach($articles as $item)
                    
                        <li class="col-md-4 col-sm-6">
                            <div class="blog-inter">
                            <div class="postimg">
                                @if (!empty($item->image) && file_exists(public_path() . '/images/articles/' .  $item->image))
                                    <img src="{{asset('images/articles/' . $item->image)}}" alt="{{$item->title}}">
                                @else
                                    <img src="{{asset('images/noImg.jpg')}}" alt="{{$item->title}}">
                                @endif
                                
                                <div class="date"><span>{{ Carbon\Carbon::parse($item->created_at)->format('d.m.Y') }}</span></div>
                                </div>
                                <div class="post-header">
                                    <h4><a href="{{route('page.articleDetails', ['article'=>$item->alias])}}">{{ strlen($item->title) > 110 ? mb_substr($item->title, 0, 110) . '...':  $item->title}}</a></h4>

                                </div>
                                <div class="postmeta">
                                    <p>{{ strlen($item->description) > 85 ? mb_substr($item->description, 0, 85) . '...':  $item->description}}</p>
                                    <div class="readmore"><a href="{{route('page.articleDetails',['article'=>$item->alias])}}">{{__('words.details')}}</a></div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <!-- blog end -->

            <!-- Pagination -->
            <div class="pagiWrap">
                <div class="row">
                    {{ $articles->links("paginate") }}
                </div>
            </div>
        </div>

        </div>
    </div>
    <!--inner-content end--> 
@endsection