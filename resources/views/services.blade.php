@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
          <h3>{{__('words.nav_services')}}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 
          
          <!-- services area start -->
          <div class="practice-wrap">
            <ul class="row">
              @foreach($services as $item)
                <li class="col-md-3 col-sm-6">
                  <div class="practiceImg">
                      @if(!empty($item->image) && file_exists(public_path() . '/images/services/' . $item->image))
                          <img src="{{asset('images/services/' . $item->image)}}" alt="{{$item->title}}">
                      @else
                          <img src="{{asset('images/noImg.jpg')}}" alt="{{$item->title}}">
                      @endif
                  </div>
                  <div class="pracInfo">
                    <h3><a href="{{route('page.serviceDetails',['service'=>$item->alias])}}">{{$item->title}}</a></h3>
                      <p>{{mb_substr($item->description, 0, 85)}}...</p>
                    <div class="readmore"><a href="{{route('page.serviceDetails',['service'=>$item->alias])}}">{{__('words.details')}}</a></div>
                  </div>
                </li>
              @endforeach

            </ul>
          </div>
          <!-- services area end -->
          
        </div>
    </div>
    <!--inner-content end--> 
      

@endsection