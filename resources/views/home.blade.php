@extends('layouts.main')
@section('content')
  <!-- Revolution slider start -->
  <div class="tp-banner-container">
    <div class="tp-banner">
      <ul>
        <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"> <img alt="" src="images/dummy.png" data-lazyload="images/banner.jpg">
          <div class="caption lft large-title tp-resizeme slidertext2" data-x="left" data-y="150" data-speed="600" data-start="1000">{!! __('words.first_slide_title') !!}</div>
          <!-- <div class="caption lfb large-title tp-resizeme slidertext6" data-x="left" data-y="270" data-speed="600" data-start="2000">{!! __('words.first_slide_description') !!}</div> -->
          <div class="caption lfl large-title tp-resizeme slidertext7" data-x="left" data-y="320" data-speed="600" data-start="3000">
            <a href="{{route('page.consultation')}}">{{__('words.nav_contact')}} <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
          </div>
        </li>

        <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"> <img alt="" src="images/dummy.png" data-lazyload="images/banner2.jpg">
          <div class="caption lft large-title tp-resizeme slidertext2" data-x="left" data-y="150" data-speed="600" data-start="1000">{!! __('words.second_slide_title') !!}</div>
          <div class="caption lfb large-title tp-resizeme slidertext6" data-x="left" data-y="270" data-speed="600" data-start="2000">{!! __('words.second_slide_description') !!}</div>
          <div class="caption lfl large-title tp-resizeme slidertext7" data-x="left" data-y="320" data-speed="600" data-start="3000">
            <a href="{{route('page.consultation')}}">{{__('words.nav_contact')}} <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <!-- Revolution slider end -->
    
  <!-- Welcome start -->
  <div class="welcomeWrap">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="welImg"><img src="{{asset('images/welcome-img.jpg')}}" alt="{!! __('words.about_title') !!}"></div>
        </div>
        <div class="col-md-8">
          <h1>{!! __('words.about_title') !!}</h1>
          <div>{!!__('words.about_short_description')!!}</div>
          <div class="welcome-content-box row"></div>
          <div class="readmore"><a href="{{route('page.about')}}">{{__('words.details')}}</a></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Welcome end --> 

  <!-- Servise start -->
  <div class="service-wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="headingTitle">
              <h1>{!! __('words.home_page_practices_title') !!}</h1>
              <p>{{__('words.home_page_practices_description')}}</p>
            </div>
          </div>
        </div>
        <ul class="row serv-area">
          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
                <h4>{{__('words.our_service_title1')}}</h4>
                <hr>
                <p class="content">{{__('words.our_service_description1')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-hourglass" aria-hidden="true"></i></div>
              <h4>{{__('words.our_service_title2')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description2')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-gavel"></i></div>
              <h4>{{__('words.our_service_title3')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description3')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i></div>
              <h4>{{__('words.our_service_title4')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description4')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-balance-scale" aria-hidden="true"></i></div>
              <h4>{{__('words.our_service_title5')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description5')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
              <h4>{{__('words.our_service_title6')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description6')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block ">
              <div class="service-icon"><i class="fa fa-link" aria-hidden="true"></i></div>
              <h4>{{__('words.our_service_title7')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description7')}}</p>
            </div>
          </li>

          <li class="col-md-3 col-sm-6">
            <div class="service-block">
              <div class="service-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
              <h4>{{__('words.our_service_title8')}}</h4>
              <hr>
              <p class="content">{{__('words.our_service_description8')}}</p>
            </div>
          </li>

        </ul>
      </div>
  </div>
  <!-- Servise end --> 

  @if (!empty($articles) && (count($articles) >= 3))
    <!-- Articles start -->
    <div class="articles-wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="headingTitle">
              <h1>{!! __('words.home_page_articles_title')!!}</h1>
            </div>
          </div>
        </div>

        <ul class="row articles-service">
          @foreach ($articles as $article)
            <li class="col-md-4 col-sm-6">
              <div class="col">
                <div class="articles-image">
                  @if (!empty($article->image) && file_exists(public_path() . '/images/articles/' .  $article->image))
                    <img src="{{asset('images/articles/' . $article->image)}}" class="article-small-image" alt="{{$article->title}}">
                  @else
                    <img src="{{asset('images/noImg.jpg')}}" class="article-small-image" alt="{{$article->title}}">
                  @endif
                </div>
                <div class="head"><a href="{{route('page.articleDetails', ['article' => $article->alias])}}">{{ strlen($article->title) > 95 ? mb_substr($article->title, 0, 95) . '...':  $article->title}}</a></div>
                <div class="date">{{ date('d F Y' , strtotime($article['created_at']))}}</div>
                <p>{{ strlen($article->description) > 95 ? mb_substr($article->description, 0, 70) . '...':  $article->description}}</p>
                <div class="clearfix"></div>
              </div>
            </li>
          @endforeach
        </ul>
        <div class="clearfix"></div>
      </div>
    </div>
    <!-- Articles end -->
  @endif


  <!-- Services start -->
  <div class="practice-wrap">
      <div class="container">
        <div class="headingTitle">
          <h1>{!! __('words.our_services_title') !!}</h1>
          <p>{{__('words.our_services_description')}}</p>
        </div>
        <ul class="row">
          @foreach($services as $item)
            <li class="col-md-3 col-sm-6">

              <div class="practiceImg">
                @if(!empty($item->image) && file_exists(public_path() . '/images/services/' . $item->image))
                  <img src="{{asset('images/services/' . $item->image)}}" alt="{{$item->title}}">
                @else
                  <img src="{{asset('images/noImg.jpg')}}" alt="{{$item->title}}">
                @endif
              </div>

              <div class="pracInfo">


                <h3><a href="{{route('page.serviceDetails', ['service' => $item->alias])}}">{{$item->title}}</a></h3>
                <p>{{mb_substr($item->description, 0, 50)}}...</p>
                <div class="readmore"><a href="{{route('page.serviceDetails', ['service' => $item->alias])}}">{{__('words.details')}}</a></div>
              </div>
            </li>
          @endforeach
        </ul>
      </div>
  </div>
  <!-- Services-wrap end -->

@endsection