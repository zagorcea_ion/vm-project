@extends('layouts.main')
@section('content')

    <!--Inner Heading end-->

    <!--inner-content start-->
    <div class="inner-content">
        <div class="container">

            <!-- contact start -->
            <div class="contactWrap contact-form">
                <h1>Felicitări !!!</h1>
                <p>În scurt timp te vom contacta. Vă mulțumim că ați ales serviciile noastre.</p>

            </div>

            <div class="success-message">
                <img src="{{asset('images/success-icon-10.png')}}" alt="success message">
            </div>
        </div>
    </div>


@endsection

