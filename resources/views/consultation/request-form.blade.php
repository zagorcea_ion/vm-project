@extends('layouts.main')
@section('content')
<div class="inner-heading">
    <div class="container">
        <h3>CONSULTANȚĂ ONLINE</h3>
    </div>
</div>

<div class="inner-content">
    <div class="container"> 
      
        <!-- contact start -->
        <div class="contactWrap contact-form">
            <h1>Completați câmpurile <span>de mai jos</span></h1>
            {{-- <p>Dacă ați decis să apelaţi la serviciile noastre juridice, doriţi să colaborăm sau aveți o întrebare la care credeţi că vă putem răspunde, contactați-ne completând formularul de mai jos sau trimiţând un e-mail. Vă mulțumim!</p> --}}
        <form method="post" action="{{ route('page.consultation-store') }}" id="consultation-form" style="margin-top: 30px">
            <div class="row">
                <div class="col-md-6">
                    <label for="name" style="text-align: initial">Numele</label>
                    <input name="name" class="validate[required,minSize[3],maxSize[100]]" type="text" id="name" placeholder="Numele">
                </div>
                <div class="col-md-6">
                    <label for="surname">Prenumele</label>
                    <input name="surname" class="validate[required,minSize[3],maxSize[100]]" type="text" id="surname" placeholder="Prenumele">
                </div>

                <div class="col-md-6">
                    <label for="phone">Numărul de telefon</label>
                    <input name="phone" class="validate[required,minSize[3],maxSize[50]]" type="text" id="phone" placeholder="Numărul de telefon">
                </div>

                <div class="col-md-6">
                    <label for="email">Adresa de E-mail</label>
                    <input name="email" class="validate[required,minSize[3],custom[email],maxSize[100]]" type="text" id="email" placeholder="Adresa de E-mail">
                </div>
                
                <div class="col-md-12">
                    <label for="email">Descrieți problema</label>
                    <textarea rows="4" class="validate[required,minSize[10],maxSize[500]]" name="subject" id="comments" placeholder="Descrieți problema"></textarea>
                </div>

                <div class="col-md-12">

                        <div><label>Alegeți modul de comunicare:</label></div>
                        <div class="md-radio md-radio-inline">
                          <input id="radio-phone" class="validate[required]" type="radio" name="communication_mode" value="phone">
                          <label for="radio-phone">Telefon</label>
                        </div>
                        <div class="md-radio md-radio-inline">
                          <input id="radio-viber" class="validate[required]" type="radio" name="communication_mode" value="viber">
                          <label for="radio-viber">Viber</label>
                        </div>
                        <div class="md-radio md-radio-inline">
                            <input id="radio-telegram" class="validate[required]" type="radio" name="communication_mode" value="telegram">
                            <label for="radio-telegram">Telegram</label>
                        </div>
                        <div class="md-radio md-radio-inline">
                            <input id="radio-whatsapp" class="validate[required]" type="radio" name="communication_mode" value="whatsapp">
                            <label for="radio-whatsapp">WhatsApp</label>
                        </div>
                    <hr>
                </div>
                <div class="col-md-12 service-legends">
                    <div class="col-md-7">
                        <p>Alegeți serviciul juridic:</p>
                    </div>
                    <div class="col-md-2">
                        <p>Tarif per oră</p>
                    </div>
                    <div class="col-md-2">
                        <p>Ore</p>
                    </div>
                    <div class="col-md-1">
                        <p>Suma</p>
                    </div>

                </div>


                @foreach ($services as $item)
              
                    <div class="col-md-12  col-xs-12 service-legends-mobile">
                        <strong>Alegeți serviciul juridic:</strong>
                        <hr>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        @if (in_array($loop->index, [1, 2]))
                            <hr class="mobile-service-legends-hr">
                        @endif

                        <div class="col-md-7 col-xs-12">
                            <input id="consultation-{{ $item->id }}" class="validate[required] checkbox-custom js-service-checbox" name="services[]" type="checkbox" value="{{ $item->id }}">
                            <label for="consultation-{{ $item->id }}" class="checkbox-custom-label">{{ $item->title }}</label>
                        </div>

                        <div class="col-md-2 col-xs-12">
                            <strong class="mobile-service-legends-rate">Tarif per oră:</strong>
                            <p id="price-per-hour-{{ $item->id }}" class="hour-rate" data-price-per-hour="{{ $item->price }}">{{ $item->price }} €</p>
                        </div>

                        <div class="col-md-2 col-xs-12">
                            <strong class="mobile-service-legends-hours">Ore: </strong>
                            <div class="qtySelector text-center">
                                <i class="fa fa-minus decreaseQty" data-id-service="{{ $item->id }}"></i>
                                <input type="text" class="qtyValue" value="0"  name="services_hours_{{ $item->id }}" />
                                <i class="fa fa-plus increaseQty" data-id-service="{{ $item->id }}"></i>
                            </div>
                        </div>

                        <div class="col-md-1 col-xs-12">
                            <strong  class="mobile-service-legends-sum">Suma: </strong>
                            <span class="sum-total-service" id="sum-total-service-{{ $item->id }}" data-sum-total-service="0">0</span><span> €</span>
                        </div>

                    </div>

                @endforeach
            
                <div class="col-md-12 col-xs-12 total-sum">
                    <div class="col-md-10 col-xs-10"><strong>Total</strong></div>
                    <div class="col-md-2 col-xs-2"><strong id="total-sum">0</strong> <strong class="currency-sign"> €</strong></div>
                </div>

                <div class="col-md-12 col-xs-12" style="margin-top: 30px">
                    <div>
                        <input id="terms-of-use" class="validate[required] checkbox-custom" name="checkbox-1" type="checkbox" style="width: 95%">
                        <label for="terms-of-use" class="checkbox-custom-label">Sunt de acord cu <a href="{{ route('page.terms-of-use') }}">{{ __('words.nav_terms_of_use') }}</a> și <a href="{{ route('page.privacy-policy') }}">{{ __('words.nav_privacy_policy') }}</a> </label>
                    </div>
                </div>

                <div class="ta-center">
                    <button class="button" type="submit" style="margin-top: 30px;">Trimite</button>
                </div>
            </div>
            </form>
        </div>
        <!-- contact end --> 
        
    </div>
</div>

<script src="{{ asset('js/consultation-request.js') }}"></script>

@endsection 