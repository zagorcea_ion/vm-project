@extends('layouts.main')
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>{{ __('consultation.info_header') }}</h3>
        </div>
    </div>
    <!--Inner Heading end--> 
      
    <!--inner-content start-->
    <div class="inner-content">
        <div class="container"> 
          
            <!-- contact start -->
            <div class="contactWrap contact-form">
                <h1 style="text-transform: none">{!! __('consultation.info_title') !!}</h1>
                <p>{{ __('consultation.info_description') }}</p>
                <section>
                    <div class="services-grid">
                        <div class="service service1">
                            <i class="fa fa-list-alt"></i>
                            <h4>{{ __('consultation.step_title_1') }}</h4>
                            <p>{{ __('consultation.step_description_1') }}</p>
                        </div>

                        <div class="service service1">
                            <i class="fa fa-credit-card"></i>
                            <h4>{{ __('consultation.step_title_2') }}</h4>
                            <p>{{ __('consultation.step_description_2') }}</p>
                        </div>

                        <div class="service service1">
                            <i class="fa fa-comments"></i>
                            <h4>{{ __('consultation.step_title_3') }}</h4>
                            <p>{{ __('consultation.step_description_3') }}</p>
                        </div>
                    </div>
                </section>

                <div class="ta-center">
                    <a href="{{ route('page.consultation-request') }}" class="button">{{ __('consultation.go_to_form_button') }}</a>  
                </div>
                
            </div>
            <!-- contact end --> 

            <div class="contactWrap contact-form" style="margin-top: 50px; text-align: left;">
                <div class="col-md-7">
                    <h2>{{ __('consultation.additional_info_title') }}</h2>

                    <ul class="social-network social-circle">
                        <li><div class="ico-viber"><img src="{{ asset('icons/viber.svg') }}"></div></li>
                        <li><div class="ico-whatsapp"><img src="{{ asset('icons/whatsapp.svg') }}"></div></li>
                        <li><div class="ico-telegram"><img src="{{ asset('icons/telegram.svg') }}"></div></li>
                        <li><div class="ico-telephone"><img src="{{ asset('icons/telephone.svg') }}"></div></li>
                    </ul>

                    <div style="margin-top: 15px">
                        {!! __('consultation.additional_info_description') !!}
                    </div>
                   
                    
                </div>
                <div class="col-md-5">
                    <img src="{{ asset('images/consultation.jpg') }}"  class="consultation-image-online" style="height: 380px; margin-top: 15px;">
				</div>
            </div>
            
        </div>
    </div>
@endsection 