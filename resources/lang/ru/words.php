<?php
return [

    //navigation
    'nav_home'     => 'Главная',
    'nav_about'    => 'О нас',
    'nav_services' => 'Услуги',
    'nav_articles' => 'Статьи',
    'nav_contact'  => 'Контакты',

    //home slider
    'first_slide_title'        => 'Welcome To Global <br>Business LawFirm',
    'first_slide_description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla<br/>efficitur consequat erat, id dignissim lacus.',

    'second_slide_title'       => 'Welcome To Global <br>Business LawFirm',
    'second_slide_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla<br/>efficitur consequat erat, id dignissim lacus.',

    //about
    'about_title'             => 'welcome to <span>Business Law Firm</span>',
    'about_short_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat. Nulla eget bibendum urna, et vehicula ante. Donec et diam sodales, 
                                  pellentesque est a, posuere ex. Curabitur mattis viverra semper. Curabitur lacus tortor, posuere id pharetra in, tincidunt non mauris. Vivamus aliquam, neque bibendum 
                                  pulvinar rutrum, purus mi venenatis urna',
    'about_description'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem augue, mollis at tincidunt a, feugiat et justo. Integer finibus est eget turpis tincidunt, vel auctor 
                                  turpis tincidunt. Morbi eget ligula egestas, varius elit a, congue nisi. In purus ex, viverra sed velit sit amet, elementum luctus lacus. Suspendisse nec fermentum erat. 
                                  Etiam in ultricies ante. Duis et ipsum a nulla congue faucibus ut feugiat quam. Sed ut orci ac eros vehicula gravida sit amet nec ante. Nulla facilisi. Sed vel sodales lacus. 
                                  Integer diam diam, mollis ac molestie ut, commodo sed sem. Nunc efficitur lacus non congue pretium. Curabitur eu nulla ac risus laoreet vestibulum. Etiam euismod arcu lacus, 
                                  non laoreet nulla placerat nec. Donec eu velit enim. Quisque ultricies est nunc, a iaculis enim viverra at. <br/>
                                  <br/>
                                  Quisque sodales bibendum elit nec fermentum. Suspendisse potenti. Quisque tortor justo, vehicula eu massa ac, condimentum venenatis elit. Duis accumsan tincidunt risus at 
                                  fermentum. Vivamus vel mattis turpis. Nullam vel augue elementum, molestie neque et, varius turpis. Donec sagittis orci lacus. Etiam auctor metus vitae elementum luctus. 
                                  Morbi sit amet eros ac ex sollicitudin rutrum. Cras pharetra diam sit amet neque aliquet feugiat. Nunc sagittis luctus erat, ut tincidunt nisl vulputate quis. Cras euismod 
                                  purus vel bibendum tempor',

    'about_first_block_title'        => 'Request A Lawyer',
    'about_first_block_description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat.',

    'about_second_block_title'       => 'Case Investigation',
    'about_second_block_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat.',

    'about_third_block_title'        => 'Search Directory',
    'about_third_block_description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat.',

    //Our services
    'our_services_title'       => 'Наши <span>услуги</span>',
    'our_services_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat. Nulla eget bibendum urna, et vehicula ante. Donec et diam sodales, pellentesque 
                                   est a, posuere ex. Curabitur mattis viverra semper.',

    'our_service_title1'       => 'FREE CONSULTING',
    'our_service_description1' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title2'       => 'SPECIAL SERVICES',
    'our_service_description2' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title3'       => 'DISCUSS STRATEGY BUILDS',
    'our_service_description3' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title4'       => 'MEDIATION',
    'our_service_description4' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title5'       => 'CILVIL LAW',
    'our_service_description5' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title6'       => 'FAMILY DISPUTES',
    'our_service_description6' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title7'       => 'CRIMINAL CHARGES',
    'our_service_description7' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    'our_service_title8'       => 'BANKRUPTCY',
    'our_service_description8' => 'Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.',

    //Home page articles
    'home_page_articles' => 'Юридические <span>статьи</span>',

    //Home page practices
    'home_page_practices_title'       => 'ОБЛАСТЬ <span>ПРАКТИКИ</span>',
    'home_page_practices_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat. Nulla eget bibendum urna, et vehicula ante. Donec et diam sodales,
                                          pellentesque est a, posuere ex. Curabitur mattis viverra semper.',

    //Contact page
    'contact_title'       => 'Связаться<span> с нами</span>',
    'contact_description' => 'Dacă ați decis să apelaţi la serviciile noastre juridice, doriţi să colaborăm sau pur şi simplu aveti o întrebare la care credeţi că vă putem răspunde, vă rugăm să ne contactaţi 
                              completând formularul de mai jos sau trimiţând un e-mail direct la una din adresele de e-mail de mai jos. Vă mulțumim!',

    'contact_form_name'    => 'Полное имя',
    'contact_form_phone'   => 'Номер телефона',
    'contact_form_email'   => 'E-mail',
    'contact_form_subject' => 'Tема',


    //footer data
    'footer_contact_title'       => 'Связаться<span> с нами</span>',
    'footer_contact_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque bibendum vestibulum leo vel tempus. Vivamus sit amet felis ac est pharetra dapibus ut ac turpis. Vivamus quis 
                                     rhoncus nisi. In elit dolor, semper vel dapibus ut, consectetur non massa. Phasellus ut sem ligula.',
    'footer_contact_information' => 'Контактная информация',
    'footer_contact_address'     => 'Бельцы, ул. Мира 38<span>эт. 3, офис. 303</span>',

    //web site data
    'site_description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s dummy text ever since the 1500s, when an unknown printer took a galley',
    'site_name'        => 'Violina M.Munteanu',

    //simple words
    'navigation' => 'Меню',
    'practice' => 'Практика',

    //buttons
    'contact_us' => 'Связаться с нами',
    'details'    => 'Подробние',
    'send'       => 'Отправить',
];