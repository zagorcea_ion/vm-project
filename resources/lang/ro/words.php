<?php

return [

    //navigation
    'nav_home'     => 'Acasă',
    'nav_about'    => 'Despre noi',
    'nav_services' => 'Servicii',
    'nav_articles' => 'Articole',
    'nav_contact'  => 'CONSULTANȚĂ',
    'nav_terms_of_use' => 'Termenii de Utilizare',
    'nav_privacy_policy' => 'Politica de Confidențialitate',

    //home slider
    'first_slide_title'        => 'Welcom to Law Office <br>Violina Munteanu',
    'first_slide_description'  => '',

    'second_slide_title'       => 'Cine caută adevărul <br>își asumă riscul de a-l găsi.',
    'second_slide_description' => '"Isabel Allende"',

    //about
    'about_title'             => 'welcome to <span>Law Office</span>',
    'about_short_description' => '<p>Cabinetul avocatului a fost fondat de avocatul Violina Munteanu în anul 2011.</p>
									<p>Prestăm asistență juridică calificată și manifestăm atitudine individuală în problema fiecăruia.</p>
									<p>Principiul de bază în relațiile cu clienții este sinceritatea dintre client avocat. Scopul acțiunilor noastre este ca drepturile clientului să fie restabilite în condițiile legii, prin stoparea ilegalităților.</p> 
                                    <p>Asistenţa juridică, pe care o prestăm asigură un nivel profesional ridicat şi calificat, bazându-ne nu doar pe o manifestare de dorinţă în acest sens, ci mai ales pe experienţa acumulată de-a lungul timpului. Toate acțiunile întreprinse, sunt în limitele legii și în interesul clientului. </p>
                                    <p>Succesul pentru noi reprezintă calitatea serviciului prestart și satisfacția clientului care beficiază de serviciile noastre.</p> ',

    'about_description'       => '<p>Cabinetul avocatului a fost fondat de avocatul Violina Munteanu în anul 2011.</p>
									<p>Prestăm asistență juridică calificată și manifestăm atitudine individuală în problema fiecăruia.</p>
									<p>Principiul de bază în relațiile cu clienții este sinceritatea dintre client avocat. Scopul acțiunilor noastre este ca drepturile clientului să fie restabilite în condițiile legii, prin stoparea ilegalităților.</p> 
                                    <p>Asistenţa juridică, pe care o prestăm asigură un nivel profesional ridicat şi calificat, bazându-ne nu doar pe o manifestare de dorinţă în acest sens, ci mai ales pe experienţa acumulată de-a lungul timpului. Toate acțiunile întreprinse, sunt în limitele legii și în interesul clientului. </p>
                                    <p>Succesul pentru noi reprezintă calitatea serviciului prestart și satisfacția clientului care beficiază de serviciile noastre.</p>
                                    <p>Asistenţa juridică o prestăm contra plată, inclusiv consultanța. La încheierea contractului de asistență juridică dintre client – avocat, sunt stabilite reguli clare, ce se impun a fi respecate reciproc. Onorariul cu clienții este stabilit în funcție de complexitatea cazului și volumul de lucru efectuat de avocat. Pentru noi timpul este valoros și îl folosim rațional.</p>
                                    <p>Profesia de avocat este liberă şi independentă, cu organizare şi funcţionare autonomă, în condiţiile legi şi ale statutului profesiei de avocat. Activitatea avocatului nu este activitate de întreprinzător. Orice persoană are dreptul să îşi aleagă în mod liber avocatul pentru a fi consultată şi reprezentată de acesta în materie juridică. Statul asigură accesul la asistenţa juridică calificată tuturor persoanelor în condiţiile legi. Persoanele fizice şi juridice sânt în drept să beneficieze, de asistenţa juridică a oricărui avocat  în bază de acord al părţilor.</p>',

    //Our services
    'our_services_title'       => 'Serviciile <span>noastre</span>',
    'our_services_description' => 'Avantajul principal al nostru este experiența și profesionalismul. Serviciile juridice pe care le prestăm de aproape 10 ani de activitate se bazează pe principii deontologice și o relație bună cu clienții noștri.',

    'our_service_title1'       => 'Consultanță juridică',
    'our_service_description1' => 'Consultanța constă din asistenţă juridică primară ce se acordă pentru orice întrebare sau adresare a clientului.',

    'our_service_title2'       => 'Asistenţa juridică calificată',
    'our_service_description2' => 'Cauze penale, cauze civile, cauze contravenţionale, cauze administrative și CEDO.',

    'our_service_title3'       => 'Genul de asistență juridică',
    'our_service_description3' => 'Întocmirea documentelor cu caracter juridic, reprezentarea intereselor clienților în instanţele de judecată și alte instituții.',

    'our_service_title4'       => 'Alte acțiuni',
    'our_service_description4' => 'Avocatul acordă persoanelor fizice, juridice şi alte genuri de asistenţă juridică, ne-interzise de lege.',

    'our_service_title5'       => 'Certificarea înscrisurilor',
    'our_service_description5' => 'În procedura de acordare a asistenţei juridice, avocatul poate adeveri copii de pe actele necesare.',

    'our_service_title6'       => 'CEDO',
    'our_service_description6' => 'Intentarea procedurii și depunerea cererii la Curtea Europeană a Drepturilor Omului în materia drepturilor omului.',

    'our_service_title7'       => 'Calitatea serviciilor noastre',
    'our_service_description7' => 'Asistenţa juridică acordată de avocat corespunde bunelor practici profesionale în materie juridică.',

    'our_service_title8'       => 'Onorariu avocat',
    'our_service_description8' => 'Avem o politică tarifară diversificată, practicând atât onorariu fixe, onorariu orare cât și onorariu de succes.',

    //Home page articles
    'home_page_articles_title' => 'Articole <span>juridice</span>',

    //Home page practices
    'home_page_practices_title'       => 'DOMENIUL DE <span>ACTIVITATE</span>',
    'home_page_practices_description' => 'Profesia de avocat este exercitată de persoane calificate şi abilitate, conform legii, să pledeze şi
                                          să acţioneze în numele clienţilor lor şi să-i reprezinte în materie juridică.',


    //footer data
    'footer_contact_title'       => 'Consultație <span>ONLINE</span>',
    'footer_contact_description' => 'Dacă ați decis să apelaţi la serviciile noastre juridice, doriţi să colaborăm sau aveți o întrebare la care credeţi că vă putem răspunde, contactați-ne completând formularul de mai jos sau trimiţând un e-mail. Vă mulțumim!',
    'footer_contact_information' => 'Informatie de contact',
    'footer_contact_address'     => 'Bălți, str. A. Pușkin 62',

    //success page key
    'success_title' => 'succes',
    'success_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet nisl enim. Morbi vel urna ac eros vehicula porttitor vitae vitae mi. Cras ac dolor eu magna vestibulum suscipit in nec enim.',

    //web site data
    'site_description' => 'Cabinetul avocatului a fost fondat de avocatul Violina Munteanu în anul 2011. Aici prestăm asistență juridică calificată, însoțită de profesionalism și atitudine individuală în problema fiecărui client. Ne respectăm clienții prin acordarea de servicii calitative, de aceia oamenii ne recomandă unui cerc larg de beneficiari.',
    'site_name'        => 'Violina M.Munteanu',

    //simple words
    'navigation' => 'Navigare',
    'services'   => 'Practică',

    //buttons
    'consultation' => 'CONSULTANȚĂ',
    'details'    => 'Detaliat',
    'send'       => 'Trimite',
    'go_to_contact_form' => 'CONSULTANȚĂ ONLINE'
];
