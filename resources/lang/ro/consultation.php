<?php

return [
    //page header
    'info_header' => 'CONSULTANȚĂ ONLINE',

    //Info page
    'info_title' => 'Pentru a obține o  <span>CONSULTANȚĂ ONLINE</span><br> urmează pașii de mai jos.',
    'info_description' => 'Acest serviciu vă oferă posibilitatea de soluționare a unor situații juridice prin telefon, prin email sau prin intermediul altor aplicații audio-video de comunicare la distanță, într-un timp mult mai scurt, fără deplasare la sediul avocatului, de obținere a unor informații juridice. Este accesibil oricui de oriunde v-ați afla prin simplul click a butonului CONSULTANȚĂ ONLINE.',

    //Consultation steps
    'step_title_1' => 'Completați formularul',
    'step_description_1' => 'Сu datele solicitate, alegeți serviciul juridic dorit și modalitatea de comunicare prin care veți fi contactat de avocat.',

    'step_title_2' => 'Plata',
    'step_description_2' => 'Sigur, rapid și simplu! După completarea formularului, plata se realizează online cu cardul.',

    'step_title_3' => 'Consultanța',
    'step_description_3' => 'Veți fi contactat de avocat prin modalitatea de comunicare aleasă de Dvs. și o să beneficiați de CONSULTANȚA ONLINE.',

    //Additional section on info page
    'additional_info_title' => 'Pentru consultanțe juridice online, recomandăm și folosim cu succes',
    'additional_info_description' => '<p>Beneficiați de servicii juridice prin a primi consultanță de la avocat online și să comunicați eficient prin aplicațiile pe care le aveți la îndemână: viber, whatsApp, telegram, telefon.</p>
                                    <p>Dacă aveți documente pentru studiu, le puteți trimite pe e-mail <a href="mailto:info@lawoffice.md ">info@lawoffice.md </a> Documentele le analizăm și vă oferim opinii și soluții legale potrivite.</p>
                                    <p>Pentru clienții existenți, oferim posibilitatea să achite online cu cardul onorariu stabilit anterior prin contract.</p>',


    'go_to_form_button' => 'CONSULTANȚĂ ONLINE',




    'contact_form_name'    => 'Numele complet',
    'contact_form_phone'   => 'Numar de telefon',
    'contact_form_email'   => 'E-mail',
    'contact_form_subject' => 'Subiect',
  
];
