<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminConsultationNotificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admin-consultation-notification-email')
            ->from(config('app.admin_mail'), 'Violina M.Munteanu Law office')
            ->replyTo(config('app.admin_mail'), 'Violina M.Munteanu Law office')
            ->subject('Aveti o nouă cerere de consultare');
    }
}
