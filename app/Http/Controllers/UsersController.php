<?php

namespace App\Http\Controllers;

use File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;



class UsersController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('articles.index');
        }
        return view('login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function logout_form()
    {
        return view('logout');
    }

    public function authenticate(Request $request)
    {
        $remember = $request->input('remember');
        if (Auth::attempt(['login' => $request->login, 'password' => $request->password], $remember)) {
            return response()->json([
                'message' => "User was successfully added"
            ], 201);
        }

        return response()->json([
            'message' => "Error"
        ], 422);
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return view('admin/users/form', User::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->validate($request, [
            'login' => 'required|unique:users,login,'.$request['id'],
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|unique:users,email,'.$request['id'],
        ]);

        if(isset($input['image'])){
            if(isset($input['image']) && !empty($input['old_image'])){
                File::delete(public_path() . '/images/users/' . $input['old_image']);
            }

            $image = str_replace('data:image/png;base64,', '', $input['image']);
            $image = str_replace(' ', '+', $image);

            $input['image'] = uniqid() . '.png';
            $path = public_path() . '/images/users/' . $input['image'];

            File::put($path, base64_decode($image));
        }

        $user = User::findOrFail($id);
        $user->fill($input);
        $user->update();

        return response()->json([
            'message' => "Person was updated"
        ], 201);

    }

    public function destroy($id)
    {
        //
    }

    /**
     * Show the profile page.
     *
     * @return view
     */
    public function profile()
    {
        return view('admin.users.profile', ['user' => User::findOrFail(Auth::user()->id), 'activePage' => "profile"]);
    }

    /**
     * Display change password form.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password($id)
    {
        return view('admin.users.formChangePassword', User::findOrFail($id));
    }

    /**
     * Change user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, $id)
    {
        if($request->ajax()){
            $input = $request->all();

            if(Auth::user()->type == 'admin'){
                $this->validate($request, [
                    'password' => 'required',
                ]);
            }else{
                $this->validate($request, [
                    'password' => 'required',
                    'old_password' => 'required',
                    'confirm_password' => 'required',
                ]);

                if($input['password'] != $input['confirm_password']){
                    return response()->json([
                        'message' => "Parolele nu sunt identice"
                    ], 422);
                }

                $user = User::findOrFail(Auth::user()->id);
                if (!Hash::check($input['old_password'], $user['password'])){
                    return response()->json([
                        'message' => "Parola veche introdusă incorect"
                    ], 422);
                }
            }

            $user = User::findOrFail($id);
            $user->fill($input);
            $user->update();

            return response()->json([
                'message' => "Parola a fost modificată cu succes"
            ], 201);
        }
    }


}
