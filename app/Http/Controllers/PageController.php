<?php

namespace App\Http\Controllers;

use App\Article;
use App\Call;
use App\Mail\ContactNotificationEmail;
use App\Practice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    private $footerServices = [];
    private $perPage = 12;

    public function __construct()
    {
        $this->footerServices = Practice::where('lang', App::getLocale())->orderby('priority', 'asc')->take(5)->get();
    }

    /**
     * Show home page.
     * @return view
     */
    public function index()
    {
        $data['active_page'] = 'homePage';
        $data['services'] = Practice::where('lang', App::getLocale())->orderby('created_at', 'desc')->take(8)->get();
        $data['articles'] = Article::where('lang', App::getLocale())->orderby('created_at', 'desc')->take(6)->get();
        $data['footerServices'] = $this->footerServices;

        return view('home', $data);
    }

    /**
     * Show about page.
     * @return view
     */
    public function about()
    {
        $data['title'] = __('words.site_name') . ' - ' . __('words.nav_about');
        $data['active_page'] = 'aboutUs';
        $data['footerServices'] = $this->footerServices;

        return view('about', $data);
    }

    /**
     * Show services page.
     * @return view
     */
    public function services()
    {
        $data['title'] = __('words.site_name') . ' - ' . __('words.nav_services');
        $data['active_page'] = 'services';
        $data['services'] = Practice::where('lang', App::getLocale())->orderby('created_at', 'desc')->get();
        $data['footerServices'] = $this->footerServices;

        return view('services', $data);
    }

    /**
     * Show service details page.
     * @param spring $alias
     * @return view
     */
    public function serviceDetails($alias)
    {
        $services = Practice::where('alias', $alias)->first();

        $data['title'] = __('words.site_name') . ' - ' . $services->title;
        $data['active_page'] = 'services';
        $data['description'] = $services->description;
        $data['service'] = $services;
        $data['footerServices'] = $this->footerServices;

        return view('serviceDetails', $data);
    }

    /**
     * Show articles page.
     * @return view
     */
    public function articles()
    {
        $data['title'] = __('words.site_name') . ' - ' . __('words.nav_articles');
        $data['active_page'] = 'articles';
        $data['articles'] = Article::where('lang', App::getLocale())->orderby('created_at', 'desc')->paginate($this->perPage);
        $data['footerServices'] = $this->footerServices;

        return view('articles', $data);
    }

    /**
     * Show article details page.
     * @param spring $alias
     * @return view
     */
    public function articleDetails($alias)
    {
        $article = Article::where('alias', $alias)->first();

        $data['title'] = __('words.site_name') . ' - ' . $article->title;
        $data['active_page'] = 'articles';
        $data['description'] = $article->description;
        $data['article'] = $article;
        $data['footerServices'] = $this->footerServices;

        $data['article_og'] = [
            'url'         => route('page.articleDetails', ['article'=>$article->alias]),
            'title'       => $article->title,
            'description' => $article->description,
            'image'       => asset('public/images/articles/' . $article->image)
        ];

        return view('articleDetails', $data);
    }

    /**
     * Show login page.
     * @return view
     */
    public function login()
    {
        $data['title'] = __('words.site_name') . ' - login';
        $data['footerServices'] = $this->footerServices;

        return view('login', $data);
    }

    /**
     * Show terms of use page.
     * @return view
     */
    public function termsOfUse()
    {
        return view('terms-of-use');
    }

    /**
     * Show privacy policy page.
     * @return view
     */
    public function privacyPolicy()
    {
        return view('privacy-policy');
    }



}
