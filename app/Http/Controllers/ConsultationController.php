<?php

namespace App\Http\Controllers;

use App\Practice;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;

use App\ConsultationService;
use App\Consultation;
use App\Http\Requests\StoreConsultationRequest;
use App\Mail\AdminConsultationNotificationEmail;
use Illuminate\Support\Facades\Mail;

class ConsultationController extends Controller
{
    
    private $footerServices = [];

    public function __construct()
    {
        $this->footerServices = Practice::where('lang', App::getLocale())->orderby('priority', 'asc')->take(5)->get();
    }

    /**
     * Show consultation info page.
     * @return view
     */
    public function infoPage ()
    {
        $data['title'] = __('words.site_name') . ' - ' . __('words.nav_contact');
        $data['active_page'] = 'contact';
        $data['footerServices'] = $this->footerServices;

        return view('consultation.info', $data);
    }

    public function requestFormPage()
    {
        $data['title'] = __('words.site_name') . ' - ' . __('words.nav_contact');
        $data['active_page'] = 'contact';
        $data['footerServices'] = $this->footerServices;
        $data['services'] = ConsultationService::orderBy('priority', 'ASC')->get();

        return view('consultation.request-form', $data);

    }

    public function storeRequest(StoreConsultationRequest $request)
    {
        //prepare services array in necessary format 
        $consultationService = [];
        foreach (ConsultationService::select('id', 'price')->get() as $item) {
            $consultationService[$item->id] = $item->price;
        }

        //prepare services and hours data with total sum  
        $paid_sum = 0;
        $services = [];
         foreach ($request->services as $item) {
            $hours = $request->input('services_hours_' . $item);
            array_push($services, ['service_id' => $item, 'hours' => $hours]);
            
            $paid_sum += $consultationService[$item] * $hours;
         }

         $consultation = new Consultation();

         $consultation->name = $request->name;
         $consultation->surname = $request->surname;
         $consultation->phone = $request->phone;
         $consultation->email = $request->email;
         $consultation->subject = $request->subject;
         $consultation->subject = $request->subject;
         $consultation->communication_type = $request->communication_mode;
         $consultation->services = json_encode($services);
         $consultation->paid_sum = $paid_sum;

         if ($consultation->save()) {
            Mail::to(config('app.admin_mail'))->send(new AdminConsultationNotificationEmail());

            return response()->json([
                'message' => "Cerere a fost adăugata",
                'redirect' => route('page.consultation-success-message'),
    
            ], 200);
         }
    }

        /**
     * Show page with success message
     * @return view
     */
    public function successMessage()
    {
        $data['title'] = __('words.site_name') . ' - ' . 'Success';
        $data['active_page'] = 'Success';
        $data['footerServices'] = $this->footerServices;

        return view('consultation.success-message', $data);
    }

}
