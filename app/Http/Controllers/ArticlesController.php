<?php

namespace App\Http\Controllers;

use App\Article;
use App\Helpers\Alias;
use App\Helpers\MyImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller
{
    private $table = "articles";
    private $imageWidth = 1280;
    private $imageHeight = 720;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('admin.articles.index', ['activePage' => "articles"]);
    }

    /**
     * Data table all records
     * @return JSON
     */
    public function selectBlog()
    {
        $query = Article::select('id', 'title', 'created_at', 'image', 'description', 'lang', 'alias');

        return datatables($query)
            ->order(function ($query) {
                $columns = array(
                    1 => 'title',
                    3 => 'created_at'
                );

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })
            ->rawColumns(['actions', 'date', 'image'])
            ->addColumn('actions', 'admin.articles.actions')
            ->addColumn('image', 'admin.articles.image')

            ->addColumn('date', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.form', ['activePage' => "articles"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|unique:' . $this->table . '|max:255' ,
            'text'        => 'required',
            'lang'        => 'required',
            'description' => 'required|max:255',
        ]);

        $input = $request->all();
        $input['alias'] = Alias::generateAlias($input['title']);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $input['image'] =  uniqid() . '.' . $image->getClientOriginalExtension();

            MyImage::saveImage($image->getRealPath(), public_path('images/articles/'),  $input['image'], $this->imageWidth, $this->imageHeight);
        }

        $article = new Article();
        $article->fill($input);
        $article->save();

        return response()->json([
            'message' => "Articol a fost adăugat"
        ], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.articles.form',['activePage' => "articles", "article" => Article::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'       => 'required|unique:' . $this->table . ',title,'.$request['id'] . '|max:255',
            'text'        => 'required',
            'lang'        => 'required',
            'description' => 'required|max:255',
        ]);

        $input = $request->all();
        $input['alias'] = Alias::generateAlias($input['title']);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $input['image'] =  uniqid() . '.' . $image->getClientOriginalExtension();

            MyImage::saveImage($image->getRealPath(), public_path('images/articles/'),  $input['image'], $this->imageWidth, $this->imageHeight);

            if(isset($input['old_image']) && !empty($input['old_image'])){
                MyImage::deleteImage(public_path('images/articles/'), $input['old_image']);
            }

        }elseif(isset($input['old_image']) && !empty($input['old_image'])){
            $input['image'] = $input['old_image'];
        }

        $article = Article::findOrFail($id);
        $article->fill($input);
        $article->update();

        return response()->json([
            'message' => "Articol au fost actualizat"
        ], 201);
    }

    /**
     * Show the form for deleting a resource.
     *
     * @param  int  $id
     * @return view
     */
    public function delete($id)
    {
        return view('admin.articles.delete', Article::findOrFail($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $article = Article::findOrFail($id);

            if(isset($article['image']) && !empty($article['image'])){
                MyImage::deleteImage(public_path('images/articles/'), $article['image']);
            }

            $article->delete();

            return response()->json([
                'message' => 'Articol a fost șters'
            ], 200);
        }
        abort(404);
    }
}
