<?php

namespace App\Http\Controllers\admin\consultation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Consultation;
use App\ConsultationService;


class ConsultationRequestController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return view('admin.consultation.requests.index', ['activePage' => "consultation-request"]);
    }

    /**
     * Display a JSON listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectRequest() 
    {
        $query = Consultation::select('id', 'name', 'surname', 'phone', 'email', 'subject', 'paid', 'created_at');

        return datatables($query)
            ->order(function ($query) {
                $columns = array(
                    0 => 'surname',
                    5 => 'created_at'
                );

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })
            ->rawColumns(['full_name', 'services', 'paid', 'actions', 'date'])
        
            ->addColumn('full_name', function($query){
                return $query->surname . ' ' . $query->name;
            })

            // 'admin.consultation.requests.services'
            ->addColumn('date', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->addColumn('actions', 'admin.consultation.requests.actions')

            ->addColumn('paid', 'admin.consultation.requests.paid-state')

            ->toJson();
    }

    /**
     * Display a detail page of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function details (int $id)
    {
        //prepare services array in necessary format 
        $consultationService = [];
        foreach (ConsultationService::select('id', 'title', 'price')->get() as $item) {
            $consultationService[$item->id] = ['title' => $item->title,'price' => $item->price];
        }

        $request = Consultation::findOrFail($id);
        $services = json_decode($request->services, true);

        return view('admin.consultation.requests.details', compact('request', 'services', 'consultationService'));
    }

        /**
     * Show the form for deleting a resource.
     *
     * @param  int  $id
     * @return view
     */
    public function delete($id)
    {
        return view('admin.consultation.requests.delete', compact('id'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $service = Consultation::findOrFail($id);

            $service->delete();

            return response()->json([
                'message' => 'Cerere a fost stearsa'
            ], 200);
        }

        abort(404);
    }
}
