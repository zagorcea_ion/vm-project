<?php

namespace App\Http\Controllers\admin\consultation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ConsultationService;
use App\Http\Requests\StoreConsultationServiceRequest;

class ConsultationServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.consultation.services.index', ['activePage' => "consultation-service"]);
    }

    public function selectService() 
    {
        $query = ConsultationService::select('id', 'title', 'price', 'priority', 'created_at');

        return datatables($query)
            ->order(function ($query) {
                $columns = array(
                    0 => 'title',
                    1 => 'price',
                    2 => 'priority',
                    3 => 'created_at'
                );

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })
            ->rawColumns(['actions', 'date'])
            ->addColumn('actions', 'admin.consultation.services.actions')

            ->addColumn('date', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.consultation.services.form', ['activePage' => "consultation-service"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreConsultationServiceRequest $request)
    {
        $article = new ConsultationService();
        $article->fill($request->input());
        $article->save();

        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Serviciu a fost adăugat');

        return response()->json([
            'message' => "Serviciu a fost adăugat"
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.consultation.services.form', ['service' => ConsultationService::findOrFail($id), 'activePage' => "consultation-service"]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreConsultationServiceRequest $request, $id)
    {
        $article = ConsultationService::findOrFail($id);
        $article->fill($request->input());
        $article->save();

        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Serviciu a fost modificat');

        return response()->json([
            'message' => "Serviciu a fost modificat"
        ], 201);
    }


    /**
     * Show the form for deleting a resource.
     *
     * @param  int  $id
     * @return view
     */
    public function delete($id)
    {
        return view('admin.consultation.services.delete', compact('id'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $service = ConsultationService::findOrFail($id);

            $service->delete();

            return response()->json([
                'message' => 'Serviciul a fost șters'
            ], 200);
        }

        abort(404);
    }
}
