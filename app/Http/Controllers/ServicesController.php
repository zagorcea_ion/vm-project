<?php

namespace App\Http\Controllers;

use App\Practice;
use App\Helpers\Alias;
use App\Helpers\MyImage;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    private $table = "practices";
    private $imageWidth = 1280;
    private $imageHeight = 720;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.services.index', ['activePage' => "services"]);
    }

    /**
     * Data table all records
     * @return JSON
     */
    public function selectServices()
    {
        
        $query = Practice::select('id', 'title', 'created_at', 'image', 'description', 'lang', 'priority', 'alias');

        return datatables($query)
            ->order(function ($query) {
                $columns = array(
                    1 => 'title',
                    3 => 'created_at',
                    5 => 'priority'
                );

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })
            ->rawColumns(['actions', 'date', 'image'])
            ->addColumn('actions', 'admin.services.actions')
            ->addColumn('image', 'admin.services.image')

            ->addColumn('date', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.form', ['activePage' => "services"]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:' . $this->table . '|max:255' ,
            'text' => 'required',
            'lang' => 'required',
            'description' => 'required|max:255',
        ]);

        $input = $request->all();

        $input['alias'] = Alias::generateAlias($input['title']);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $input['image'] =  uniqid() . '.' . $image->getClientOriginalExtension();

            MyImage::saveImage($image->getRealPath(), public_path('images/services/'),  $input['image'], $this->imageWidth, $this->imageHeight);
        }

        $practice = new Practice();
        $practice->fill($input);
        $practice->save();

        return response()->json([
            'message' => "Serviciu a fost adăugata"
        ], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.services.form',['activePage' => "services", 'services' => Practice::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|unique:' . $this->table . ',title,'.$request['id'] . '|max:255',
            'text' => 'required',
            'lang' => 'required',
            'description' => 'required|max:255',
        ]);

        $input = $request->all();
        $input['alias'] = Alias::generateAlias($input['title']);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $input['image'] =  uniqid() . '.' . $image->getClientOriginalExtension();

            MyImage::saveImage($image->getRealPath(), public_path('images/services/'),  $input['image'], $this->imageWidth, $this->imageHeight);

            if(isset($input['old_image']) && !empty($input['old_image'])){
                MyImage::deleteImage(public_path('images/services/'), $input['old_image']);
            }

        }elseif(isset($input['old_image']) && !empty($input['old_image'])){
            $input['image'] = $input['old_image'];
        }

        $practice = Practice::findOrFail($id);
        $practice->fill($input);
        $practice->update();

        return response()->json([
            'message' => "Serviciu au fost actualizata"
        ], 201);
    }

    /**
     * Show the form for deleting a resource.
     *
     * @param  int  $id
     * @return view
     */
    public function delete($id)
    {
        return view('admin.services.delete', Practice::findOrFail($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $practice = Practice::findOrFail($id);

            if(isset($practice['image']) && !empty($practice['image'])){
                MyImage::deleteImage(public_path('images/services/'), $practice['image']);
            }

            $practice->delete();

            return response()->json([
                'message' => 'Serviciu a fost ștearsa'
            ], 200);
        }
        abort(404);
    }
}
