<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Practice extends Model
{
    protected $fillable = ['title', 'text', 'image', 'id_author', 'lang', 'priority', 'description', 'alias'];
}
