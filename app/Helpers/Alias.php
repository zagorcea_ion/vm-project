<?php
namespace App\Helpers;

class Alias
{

    public static function generateAlias($title)
    {
        //all delimiters
        $deleteCaracter = array(".", ",", "/", "'", "\"", "`", "_", "!", "?");

        //replace spaces
        $title = str_replace(' ',  '-', $title);

        //replace delimiters
        $title = str_replace($deleteCaracter,  '', $title);

        //replace romanian Letters
        $title = str_replace('ă',  'a', $title);
        $title = str_replace('Ă',  'A', $title);

        $title = str_replace('î',  'i', $title);
        $title = str_replace('Î',  'I', $title);

        $title = str_replace('ș',  's', $title);
        $title = str_replace('Ș',  'S', $title);

        $title = str_replace('ț',  't', $title);
        $title = str_replace('Ț',  'T', $title);

        $title = str_replace('â',  'i', $title);
        $title = str_replace('Â',  'Â', $title);

        return $title;
    }
}