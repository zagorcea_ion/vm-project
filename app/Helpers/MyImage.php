<?php
namespace App\Helpers;

use File;
use Image;

class MyImage
{

    public static function saveImage($image, $path, $name, $width = 1920, $height = 1080){

        $image_resize = Image::make($image);
        $image_resize->resize($width, $height);
        $image_resize->save($path . $name);
    }

    public static function deleteImage($path, $name){
        File::delete($path . $name);
    }

}