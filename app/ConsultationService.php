<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultationService extends Model
{
    protected $fillable=['title', 'price', 'priority'];
}
