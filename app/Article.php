<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable=['title', 'alias', 'text', 'image', 'youtube_video', 'id_author', 'lang', 'description'];
}
