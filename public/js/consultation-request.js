(function(window, $) {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    if (token) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token.content,
                'X-Shenanigans': true,
            }
        })
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

    $(".close-alert").click(function () {
        $('#message-box-success').hide();
    });
    $("#message-box-danger").click(function () {
        $('.message-box-danger').removeClass("show");
        $('.message-box-danger').addClass("hide");
    });


    var minVal = 1, maxVal = 20; // Set Max and Min values

    // Increase product quantity on cart page
    $(".increaseQty").on('click', function(){
        var $parentElm = $(this).parents(".qtySelector");

        var idService = $(this).data("id-service")

        var value = $parentElm.find(".qtyValue").val();

        if (value < maxVal) {
            value++;
        }

        if (!$("#consultation-" + idService).is(":checked")) {
            $("#consultation-" + idService).prop('checked', true);
        }

        calculateTotalSumService(idService, value);
        calculateTotalSum();

        $parentElm.find(".qtyValue").val(value);
    });

    // Decrease product quantity on cart page
    $(".decreaseQty").on('click', function(){
        var $parentElm = $(this).parents(".qtySelector");

        var idService = $(this).data("id-service")

        var value = $parentElm.find(".qtyValue").val();
        if (value > 0) {
            value--;
        }

        if ($("#consultation-" + idService).is(":checked") && value == 0) {
            $("#consultation-" + idService).prop('checked', false);
        }

        calculateTotalSumService(idService, value);
        calculateTotalSum();

        $parentElm.find(".qtyValue").val(value);
    });


    function calculateTotalSumService(idService, hours) {

        var pricesPerHour = $('#price-per-hour-' + idService).data('price-per-hour');
        var totalSumService = pricesPerHour * hours;

        $('#sum-total-service-' + idService).text(totalSumService);

        return $('#sum-total-service-' + idService).attr('data-sum-total-service', totalSumService);
    }

    function calculateTotalSum() {
    var totalSum = 0;

    $('.sum-total-service').each(function(){
        totalSum +=  Number($(this).attr("data-sum-total-service"));
    });

    return $('#total-sum').text(totalSum);
    }

    $('.js-service-checbox').on('click', function(){

        var idService = $(this).val();

        var $parentCounterElm = $('.increaseQty[data-id-service="' + idService + '"]').parents(".qtySelector");

        if ($(this).is(":checked")) {
            $('.increaseQty[data-id-service="' + idService + '"]').trigger("click")
        } else {
            $parentCounterElm.find(".qtyValue").val(0);
            calculateTotalSumService(idService, 0);
            calculateTotalSum()
        }
    })

    $("#consultation-form").validationEngine({
        validateNonVisibleFields: true,
        updatePromptsPosition:true,
        promptPosition : "topLeft",
        onValidationComplete: function(form, status){
            if(status){
                $.ajax ({
                    url : $("#consultation-form").attr('action'),
                    type: 'POST',
                    data: new FormData($('#consultation-form')[0]),
                    dataType: 'JSON',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    success: function (response) {
                        if (typeof response.redirect != 'undefined' && typeof response.redirect != null) {
                            window.location.replace(response.redirect);
                        }
                    },
                    error: function (error) {
                        onSaveRequestError(error);
                    }
                })
            }
        }
    });
} (window, jQuery));


