(function($) { 

	"use strict";
	
	/* ================ Revolution Slider. ================ */
	if($('.tp-banner').length > 0){
		$('.tp-banner').show().revolution({
			delay:6000,
	        startheight: 550,
	        startwidth: 1040,
	        hideThumbs: 1000,
	        navigationType: 'none',
	        touchenabled: 'on',
	        onHoverStop: 'on',
	        navOffsetHorizontal: 0,
	        navOffsetVertical: 0,
	        dottedOverlay: 'none',
	        fullWidth: 'on'
		});
	}
	if($('.tp-banner-full').length > 0){
		$('.tp-banner-full').show().revolution({
			delay:6000,
	        hideThumbs: 1000,
	        navigationType: 'none',
	        touchenabled: 'on',
	        onHoverStop: 'on',
	        navOffsetHorizontal: 0,
	        navOffsetVertical: 0,
	        dottedOverlay: 'none',
	        fullScreen: 'on'
		});
	}
	

/* ================ Collaps for faqs ================ */

	$(document).ready(function() {
		$('.collaps p').hide();
		$(document).on("click", '.collaps h4', function() {
			if( $(this).hasClass('active') ){
				$(this).next('p').slideUp();
				$(this).removeClass('active');
			} else {
				$(this).next('p').slideDown();
				$(this).addClass('active');
			}
		});
	});





/* ================ sticky fix ================ */
	
	$(window).scroll(function() {
if ($(this).scrollTop() > 32){  
    $('.header').addClass("sticky");
  }
  else{
    $('.header').removeClass("sticky");
  }
});

	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){

		if ($(this).scrollTop() > 100) {

			$('.scrollToTop').fadeIn();

		} else {

			$('.scrollToTop').fadeOut();

		}

	});


	//Click event to scroll to top
	$('.scrollToTop').click(function(){

		$('html, body').animate({scrollTop : 0},800);

		return false;

	});

	
})(jQuery);

$(document).ready(function(){
	$(".dropdown").mouseover(function(){
		$(".dropdown-menu").show();
	});
	
	$(".dropdown").mouseout(function(){
		$(".dropdown-menu").hide();
	});

	
	$(".dropdown-menu").mouseout(function(){
		$(".dropdown-menu").hide();
	});

});

function onSaveRequestError(error) {
    if(!error.hasOwnProperty('statusCode')) {
        //console.error(error);

        return;
    }

    if(!error.hasOwnProperty('responseJSON')) {
        new Noty({type: 'error', layout: 'topRight', text: "Bad response", timeout:3000}).show();
       //console.error(error);

        return;
    }

    var responseBody = error.responseJSON || {};
    if(responseBody.hasOwnProperty('errors')) {
        for (var key in responseBody.errors) {
            if (responseBody.errors.hasOwnProperty(key)) {
                var errorEntry = responseBody.errors[key];
                if(Array.isArray(errorEntry)) {
                    errorEntry.forEach(function(entry) {
                        new Noty({type: 'error', layout: 'topRight', text: entry, timeout:3000}).show();
                    });
                } else {
                    new Noty({type: 'error', layout: 'topRight', text: errorEntry, timeout:3000}).show();
                }
            }
        }
    } else if(responseBody.hasOwnProperty('message')){
        new Noty({type: 'error', layout: 'topRight', text: responseBody.message, timeout:3000}).show();
    } else {
        new Noty({type: 'error', layout: 'topRight', text: "Server error", timeout:3000}).show();
        //console.error(error);
    }
};

(function(window, $) {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    if (token) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token.content,
                'X-Shenanigans': true,
            }
        })
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

    $(".close-alert").click(function () {
        $('#message-box-success').hide();
    });
    $("#message-box-danger").click(function () {
        $('.message-box-danger').removeClass("show");
        $('.message-box-danger').addClass("hide");
    });
} (window, jQuery));