<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//SetLocale
Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::$languages)) {

        unset($segments[1]); //удаляем метку
    } 
    
    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != App\Http\Middleware\LocaleMiddleware::$mainLanguage){ 
        array_splice($segments, 1, 0, $lang); 
    }

    //формируем полный URL
    $url = Request::root().implode("/", $segments);
    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){    
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }

    if(substr($url, -1) == "/"){
        $url = substr_replace($url, "", -1);
    }
    
    return redirect($url); //Перенаправляем назад на ту же страницу                            

})->name('setlocale');

//Main
Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function(){
    Route::get('/', 'PageController@index')->name('page.home');
    Route::get('/about', 'PageController@about')->name('page.about');
    Route::get('/services', 'PageController@services')->name('page.services');
    Route::get('/services/{service}', 'PageController@serviceDetails')->name('page.serviceDetails');

//    Route::get('news/{news}', 'PagesController@fullNewsPage')->name('fullNewsPage');
    Route::get('/articles', 'PageController@articles')->name('page.articles');
    Route::get('/articles/{article}', 'PageController@articleDetails')->name('page.articleDetails');
    Route::get('/login', 'PageController@login')->name('login');

    //Сonsultation
    Route::get('/consultation', 'ConsultationController@infoPage')->name('page.consultation');
    Route::get('/consultation-request', 'ConsultationController@requestFormPage')->name('page.consultation-request');
    Route::post('/consultation/store-request', 'ConsultationController@storeRequest')->name('page.consultation-store');
    Route::get('/consultation/success-message', 'ConsultationController@successMessage')->name('page.consultation-success-message');

    Route::get('/terms-of-use', 'PageController@termsOfUse')->name('page.terms-of-use');
    Route::get('/privacy-policy', 'PageController@privacyPolicy')->name('page.privacy-policy');

});


//Login
Route::get('/logout', 'UsersController@logout')->name('logout');
Route::get('/logout-form', 'UsersController@logout_form')->name('logout-form');
Route::post('/admin-panel', 'UsersController@authenticate')->name('authenticate');

//Admin-panel
Route::group(['prefix'=>'admin-panel', 'middleware'=>'auth'], function (){
    Route::get('/', 'ArticlesController@index');

    //Article
    Route::post('articles/select', 'ArticlesController@selectBlog')->name('articles.list');
    Route::get('articles/{article}/delete', 'ArticlesController@delete')->name('articles.delete');
    Route::resource('articles', 'ArticlesController');

    //Service
    Route::post('services/select', 'ServicesController@selectServices')->name('services.list');
    Route::get('services/{services}/delete', 'ServicesController@delete')->name('services.delete');
    Route::resource('services', 'ServicesController');

    //Users
    Route::get('users/profile', 'UsersController@profile')->name('users.profile');
    Route::get('users/{users}/password', 'UsersController@password')->name('users.password');
    Route::post('users/{users}/change-password', 'UsersController@changePassword')->name('users.change-password');

    Route::resource('users', 'UsersController');

    //Consultation
    Route::group(['prefix'=>'consultation', 'as' => 'consultation.'], function (){
        
        //Services
        Route::resource('services', 'admin\consultation\ConsultationServiceController');
        Route::post('services/select', 'admin\consultation\ConsultationServiceController@selectService')->name('services.list');
        Route::get('services/{service}/delete', 'admin\consultation\ConsultationServiceController@delete')->name('services.delete');

        //Requests
        Route::get('requests', 'admin\consultation\ConsultationRequestController@index')->name('requests.index');
        Route::post('requests/select', 'admin\consultation\ConsultationRequestController@selectRequest')->name('requests.list');
        Route::get('requests/details/{request}', 'admin\consultation\ConsultationRequestController@details')->name('requests.details');
        Route::get('requests/{request}/delete', 'admin\consultation\ConsultationRequestController@delete')->name('requests.delete');
        Route::delete('requests/{request}', 'admin\consultation\ConsultationRequestController@destroy')->name('requests.destroy');

    });
});